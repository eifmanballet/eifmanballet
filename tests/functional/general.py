# encoding=utf-8

from selenium import webdriver
import unittest


class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.quit()

    def test_can_view_a_main_page(self):
        # Alex open a main page
        self.browser.get('http://localhost:8000')

        # He saw page title mentioned Eifman Theatre site
        self.assertIn(u'Театр Бориса Эйфмана', self.browser.title)

        self.fail('Finish this test!')

if __name__ == '__main__':
    unittest.main()
