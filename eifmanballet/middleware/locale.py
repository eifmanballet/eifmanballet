# coding=utf-8
from django.http import HttpResponsePermanentRedirect
from django.middleware.locale import LocaleMiddleware
from django.core.urlresolvers import (is_valid_path, resolve)
from django.utils import translation
from django.conf import settings
from django.utils.cache import patch_vary_headers


class PatchedLocaleMiddleware(LocaleMiddleware):
    """ Patched version of django.locale.LocaleMiddleware
        Return 301 Moved permanently instead of 302 Found
        Redirect pages with trailing slashes to page without it
    """
    def process_response(self, request, response):
        language = translation.get_language()
        language_from_path = translation.get_language_from_path(
                request.path_info, supported=self._supported_languages
        )
        prefixes = (u'/ru', u'/en', u'')

        if response.status_code == 404 and request.path_info in prefixes:
            return HttpResponsePermanentRedirect("{}/".format(request.path_info))

        if request.path_info == u'' or request.path_info == u'/':
            return HttpResponsePermanentRedirect("{}/".format(language))

        if (response.status_code == 404 and not language_from_path
                and self.is_language_prefix_patterns_used()):
            urlconf = getattr(request, 'urlconf', None)
            language_path = '/%s%s' % (language, request.path_info)
            path_valid = is_valid_path(language_path, urlconf)
            if (not path_valid and settings.APPEND_SLASH
                    and not language_path.endswith('/')):
                path_valid = is_valid_path("%s/" % language_path, urlconf)

            if path_valid:
                language_url = "%s://%s/%s%s" % (
                    'https' if request.is_secure() else 'http',
                    request.get_host(), language, request.get_full_path())
                return HttpResponsePermanentRedirect(language_url)

        if response.status_code == 404 and request.path_info.endswith('/'):
            urlconf = getattr(request, 'urlconf', None)
            trimmed_path = request.path_info[:-1]
            path_valid = is_valid_path(trimmed_path, urlconf)

            if path_valid:
                url = "%s://%s/%s" % (
                    'https' if request.is_secure() else 'http',
                    request.get_host(), trimmed_path)
                return HttpResponsePermanentRedirect(url)

        if not (self.is_language_prefix_patterns_used()
                and language_from_path):
            patch_vary_headers(response, ('Accept-Language',))
        if 'Content-Language' not in response:
            response['Content-Language'] = language
        return response