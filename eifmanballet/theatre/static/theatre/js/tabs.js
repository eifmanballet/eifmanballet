(function(window, document, $, undefined) {
    var defaults = {
        activeClass: 'tabs__header-item_active_true',
        contentClass: 'tabs__content-item_visible_yes'
    };

    $.Tabs = function($root, options) {
        this.$handlers = undefined;
        this.$items = undefined;
        this.$header = undefined;

        this.init = function() {
            this.$header = $('.tabs__header', $root);
            this.$handlers = $('.tabs__header-item', $root);
            this.$items = $('.tabs__content-item', $root);

            this._bind();
        };

        this._bind = function() {
            this.$header.on('click', '.tabs__header-item', $.proxy(this.showItem, this));
        };

        this.showItem = function(e) {
            var $target = $(e.currentTarget);
            var idx = this.$handlers.index($target);
            if ($target.hasClass('tabs__header-item_disabled_yes')) {
                return false;
            }

            this.$handlers.removeClass(options.activeClass);
            this.$items.removeClass(options.contentClass);
            $target.addClass(options.activeClass);
            this.$items.eq(idx).addClass(options.contentClass);
        };
    };

    $.fn.tabs = function(options) {
        return this.each(function() {
            var $tabs = $(this);
            var tabsData = $tabs.data();
            var instanceOpts = $.extend(
                {},
                defaults,
                options,
                tabsData
            );

            return new $.Tabs($tabs, instanceOpts).init();
        });
    };
})(window, document, jQuery, undefined);
