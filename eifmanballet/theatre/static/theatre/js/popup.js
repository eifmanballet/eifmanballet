(function(window, document, $, undefined) {
    var defaults = {};

    $.Popup = function($root, options) {
//        this.$handlers = $('.popup__handler', $root);
        this.$openedPopup = undefined;
        this.$doc = $(document);

        this.init = function() {
            this._bind();
        };

        this._bind = function() {
            $root.on('click', '.popup__handler', $.proxy(this._showPopup, this));
        };

        this._hidePopup = function() {
            if (this.$openedPopup) {
                this.$openedPopup.remove();
                this.$openedPopup = undefined;
            }
        };

        this._showPopup = function(e) {
            var $target = $(e.currentTarget);
            var targetOffset = $target.offset();
            var rootOffset = $root.offset();
            var dx = targetOffset.left - rootOffset.left;
            var dy = targetOffset.top - rootOffset.top;

            this._hidePopup();
            this.$doc.off('click.popup', $.proxy(this._hidePopup, this));

            this.$openedPopup = $target
                .find('.popup__popup')
                .clone()
                .addClass('popup__popup_visible_yes')
                .css({
                    left: dx,
                    top: dy
                })
                .appendTo($root);

            this.$doc.one('click.popup', $.proxy(this._hidePopup, this));
            return false;
        };
    };

    $.fn.popup = function(options) {
        return this.each(function() {
            var $popup = $(this);
            var sliderData = $popup.data();
            var instanceOpts = $.extend(
                {},
                defaults,
                options,
                sliderData
            );

            return new $.Popup($popup, instanceOpts).init();
        });
    };
})(window, document, jQuery, undefined);
