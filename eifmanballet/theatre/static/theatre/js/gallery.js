(function(window, document, $, undefined) {
    var defaults = {
        popupTemplate: "#gallery-popup-template"
    };
    var KEYS = {
        ESC: 27,
        ENTER: 13,
        LEFT_ARROW: 37,
        UP_ARROW: 38,
        RIGHT_ARROW: 39,
        DOWN_ARROW: 40
    };

    $.Gallery = function($root, options) {
        this.$body = $('body');
        this.thumbnails = '.gallery__thumbnail';
        this.currentItem = undefined;
        this.$thumbnails = undefined;
        this.$previewItems = undefined;
        this.$template = $($(options.popupTemplate).html());
        this.thData = [];
        this.$leftArrow = undefined;
        this.$rightArrow = undefined;
        this.$previewLeftArrow = undefined;
        this.$previewRightArrow = undefined;
        this.$closer = undefined;
        this.previewStep = undefined;
        this.previewsWidth = undefined;
        this.previewsViewport = undefined;

        this.init = function() {
            this.$thumbnails = $(this.thumbnails, $root);
            this.thData = this.$thumbnails.map(function() {
                return $(this).data();
            });

            this._bind();
        };

        this._bind = function() {
            $root.on('click', this.thumbnails, $.proxy(this.thumbnailHandler, this));
        };

        this.thumbnailHandler = function(e) {
            var $el = $(e.currentTarget);

            this.openPopup(this.$thumbnails.index($el));
        };

        this.openPopup = function(id) {
            this.initPopup();

            this.setCurrentItem(id);
            this._initKeyboard();
            return false;
        };

        this.initPopup = function() {
            var that = this;

            if (this.$popup !== undefined) {
                this.$popup.show().appendTo(this.$body);
                return;
            }

            this.$popup = this.$template;
            $('.gallery-popup', this.$popup).css('max-height', $(window).height() - 50);

            var $imageWrapper = $('.gallery-popup__image-wrapper', this.$popup);
            var wrapperMaxHeight = $(window).height() - 350;
            var wrapperCSSHeight = parseInt($imageWrapper.css('height')) || 570;
            var wrapperStyles = {
                'max-height': wrapperMaxHeight,
                'line-height': (wrapperCSSHeight > wrapperMaxHeight ? wrapperMaxHeight : wrapperCSSHeight) + "px"
            };

            $imageWrapper.css(wrapperStyles);
            this.$leftArrow = $('.gallery-popup__image-arrow_direction_left', this.$popup)
                .on('click', $.proxy(this.showPrevItem, this));
            this.$rightArrow = $('.gallery-popup__image-arrow_direction_right', this.$popup)
                .on('click', $.proxy(this.showNextItem, this));
            this.$previewLeftArrow = $('.gallery-popup__footer-arrow_direction_left', this.$popup)
                .on('click', $.proxy(this.showPrevPreview, this));
            this.$previewRightArrow = $('.gallery-popup__footer-arrow_direction_right', this.$popup)
                .on('click', $.proxy(this.showNextPreview, this));
            this.$closer = $('.gallery-popup__closer', this.$popup)
                .on('click', $.proxy(this.closePopup, this));
            this.$popup.on('click', $.proxy(this.overlayHandler, this));

            var $previews = $('.gallery-popup__previews', this.$popup);

            this.thData.each(function(id, elem) {
                $previews.append("<div class='gallery-popup__preview'><img src='" + elem.th + "' /></div>");
            });

            this.$previewItems = $('.gallery-popup__preview', $previews);

            $previews.on('click', '.gallery-popup__preview', function(e) {
                var $el = $(e.currentTarget);
                var id = that.$previewItems.index($el);
                that.setCurrentItem(id);
            });

            this.$popup.appendTo(this.$body);

            this.previewStep = this.$previewItems.outerWidth(true);
            this.previewsWidth = this.previewStep * (this.$previewItems.length - 1);
            this.previewsViewport = $previews.width();
            this._checkPreviewArrows();
        };

        this.overlayHandler = function(e) {
            if (e.target === e.currentTarget) {
                this.closePopup();
            }
        };


        this.selectPreview = function () {
            this.$previewItems
                .removeClass('gallery-popup__preview_selected_yes')
                .eq(this.currentItem)
                .addClass('gallery-popup__preview_selected_yes');
            this.checkPreviewVisible(this.currentItem);
        };

        this.checkPreviewVisible = function(id) {
            var preview = this.$previewItems.eq(id);
            var previewPosition = this.previewStep * id;
            var previewsOffset = Math.abs(parseInt(preview.css('left')));
            var overflowRight = previewPosition - this.previewsViewport - previewsOffset;
            var overflowLeft = previewPosition - previewsOffset;

            if (overflowRight > 0) {
                this.showNextPreview(null, id - 7);
            }

            if (overflowLeft < 0) {
                this.showPrevPreview(null, id);
            }
        };

        this.checkImageArrows = function () {
            var disabledClassName = 'gallery-popup__image-arrow_disabled_yes';
            this.$leftArrow.toggleClass(disabledClassName, this._isFirst());
            this.$rightArrow.toggleClass(disabledClassName, this._isLast());
        };

        this.setCurrentItem = function(id) {
            var _prevItem = this.currentItem;
            this.currentItem = id || 0;
            if (id < 0) {
                this.currentItem = 0;
            }
            if (id >= this.thData.length) {
                this.currentItem = this.thData.length - 1;
            }
            if (this.currentItem != _prevItem) {
                this.showImage(this.currentItem);
                this.selectPreview();
                this.checkImageArrows();
            }
        };

        this.showNextItem = function() {
            this.setCurrentItem(this.currentItem + 1);
        };

        this.showPrevItem = function() {
            this.setCurrentItem(this.currentItem - 1);
        };

        this.showNextPreview = function(e, id) {
            var left;
            if (id) {
                left = -this.previewStep * id;
            } else {
                left = "-=" + this.previewStep;
            }
            if (!this._isLastPreview()) {
                this.$previewItems.animate({left: left},
                    $.proxy(this._checkPreviewArrows, this)
                );
            }
        };

        this.showPrevPreview = function(e, id) {
            var left;
            if (id) {
                left = -this.previewStep * id;
            } else {
                left = "+=" + this.previewStep;
            }
            if (!this._isFirstPreview()) {
                this.$previewItems.animate({left: left},
                    $.proxy(this._checkPreviewArrows, this)
                );
            }
        };

        this._checkPreviewArrows = function() {
            var disabledClassName = 'gallery-popup__footer-arrow_disabled_yes';
            this.$previewLeftArrow.toggleClass(disabledClassName, this._isFirstPreview());
            this.$previewRightArrow.toggleClass(disabledClassName, this._isLastPreview());
        };

        this._isFirstPreview = function() {
            return parseInt(this.$previewItems.css('left')) >= 0;
        };

        this._isLastPreview = function() {
            return Math.abs(parseInt(this.$previewItems.css('left'))) >= this.previewsWidth - this.previewsViewport;
        };

        this._isFirst = function() {
            return this.currentItem === 0;
        };

        this._isLast = function() {
            return this.currentItem === this.thData.length - 1;
        };

        this.showImage = function(id) {
            var item = this.thData[id];
            $('.gallery-popup__image-photo', this.$popup)
                .attr('src', item.img);
            $('.gallery-popup__title', this.$popup).text(item.title);
        };

        this.closePopup = function() {
            this.$popup
                .hide()
                .detach();
            this._restoreKeyboard()
        };

        this._initKeyboard = function() {
            var that = this;

            this.$body.on("keyup.gallery", function(e) {
                switch (e.which) {
                    case KEYS.ESC:
                        that.closePopup();
                        break;
                    case KEYS.LEFT_ARROW:
                        that.showPrevItem();
                        break;
                    case KEYS.RIGHT_ARROW:
                        that.showNextItem();
                        break;
                }
            });
        };

        this._restoreKeyboard = function () {
            this.$body.off("keyup.gallery");
        };
    };

    $.fn.gallery = function(options) {
        return this.each(function() {
            var $gallery = $(this);
            var galleryData = $gallery.data();
            var instanceOpts = $.extend(
                {},
                defaults,
                options,
                galleryData
            );

            return new $.Gallery($gallery, instanceOpts).init();
        });
    };
})(window, document, $, undefined);
