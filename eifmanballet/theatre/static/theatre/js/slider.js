(function(window, document, $, undefined) {
    var defaults = {
        item: '.slider__item',
        itemsInView: 1,
        showPager: true
    };

    $.Slider = function($root, options) {
        var that = this;
        this.$items = undefined;
        this.$wrapper = undefined;
        this.$arrows = undefined;
        this.step = undefined;
        this.$content = undefined;
        this.$pager = undefined;
        this.extremums = [true, true];
        this.currentSlide = 0;

        this.init = function() {
            this.$items = $(options.item, $root);
            this.step = this.$items.outerWidth(true);
            this.$wrapper = $('.slider__wrapper', $root);
            this.$content = $('.slider__content', $root);
            this.$arrows = $('.slider__arrow', $root);
            this.$leftArrow = this.$arrows.filter(".slider__arrow_direction_left");
            this.$rightArrow = this.$arrows.filter(".slider__arrow_direction_right");
            if (options.showPager && this.$items.length > 1) {
                this.$pager = $("<div class='slider__pager'></div>");
                for (var i = 0, l = this.$items.length; i < l; i++) {
                    this.$pager.append("<span class='slider__pager-item'></span>");
                }
                this.$pager.appendTo($root);
            }
            if (this.$items.length === 1) {
                this.$arrows.hide();
            }
            this._bind();
            this.checkState();
        };

        this.pagerHandler = function(e) {
            var id = $(e.target).index();

            this.slideTo(id);;
        };

        this.slideLeft = function () {
            if (this.extremums[1]) {
                return;
            }
            this.slide(true);
        };

        this.slideRight = function () {
            if (this.extremums[0]) {
                return;
            }
            this.slide(false);
        };

        this._bind = function() {
            this.$leftArrow.on('click', $.proxy(this.slideLeft, this));
            this.$rightArrow.on('click', $.proxy(this.slideRight, this));
            if (this.$pager) {
                this.$pager.on('click', '.slider__pager-item', $.proxy(this.pagerHandler, this));
            }
        };

        this.slide = function(reverse) {
            var direction;
            if (reverse) {
                direction = "+=";
                this.currentSlide--;
            } else {
                direction = "-=";
                this.currentSlide++;
            }

            if (this.currentSlide < 0) {
                this.currentSlide = 0;
                return;
            }
            if (this.currentSlide >= this.$items.length) {
                this.currentSlide = this.$items.length - 1;
                return;
            }

            this.$wrapper.animate({
                left: direction + this.step + 'px'
            }, $.proxy(this.checkState, this));
        };

        this.slideTo = function(idx) {
            this.currentSlide = idx;

            this.$wrapper.animate({
                left: "-" + this.step * idx + 'px'
            }, $.proxy(this.checkState, this));
        };

        this.highlightPager = function() {
            this.$pager
                .find(".slider__pager-item")
                .removeClass('slider__pager-item_selected_yes')
                .eq(this.currentSlide)
                .addClass('slider__pager-item_selected_yes');
        };

        this.checkState = function() {
            var position = this.$wrapper.position().left;
            var contentWidth = this.getContentWidth();

            this.extremums[1] = position >= 0;
            this.extremums[0] = Math.abs(position) >= contentWidth - this.$content.width();

            this.$rightArrow.toggleClass('slider__arrow_disabled_yes', this.extremums[0]);
            this.$leftArrow.toggleClass('slider__arrow_disabled_yes', this.extremums[1]);

            if (this.$pager) {
                this.highlightPager();
            }
        };

        this.getContentWidth = function() {
            return (this.$items.length - (options.itemsInView - 1)) * this.step;
        };
    };

    $.fn.slider = function(options) {
        return this.each(function() {
            var $slider = $(this);
            var sliderData = $slider.data();
            var instanceOpts = $.extend(
                {},
                defaults,
                options,
                sliderData
            );

            return new $.Slider($slider, instanceOpts).init();
        });
    };
})(window, document, jQuery, undefined);
