"use strict";

(function(window, document, $, undefined) {
    var defaults = {};

    $.SchedulePopup = function($root, options) {
        this.$openedPopup = undefined;
        this.$body = $('body');
        this.$shadow = undefined;
        this.$cloned = undefined;

        this.init = function() {
            this._bind();
        };

        this._bind = function() {
            $root.on('click', $.proxy(this._showPopup, this));
        };

        this._hidePopup = function() {
            if (this.$openedPopup) {
                this.$openedPopup.remove();
                this.$openedPopup = undefined;
                this.$shadow.removeClass('shadow_visible_yes');
            }
        };

        this._showPopup = function(e) {
            this.$cloned = $root.clone().addClass('schedule-item_mode_popup');
            this.$shadow = this.$shadow || $('<div class="shadow"></div>')
                .appendTo(this.$body);

            this.$openedPopup = this.$cloned.appendTo(this.$shadow);
            this.$shadow.addClass('shadow_visible_yes');

            this.$body.one('click.popup', $.proxy(this._hidePopup, this));
            return false;
        };
    };

    $.fn.schedulePopup = function(options) {
        return this.each(function() {
            var $popup = $(this);
            var sliderData = $popup.data();
            var instanceOpts = $.extend(
                {},
                defaults,
                options,
                sliderData
            );

            return new $.SchedulePopup($popup, instanceOpts).init();
        });
    };

})(window, document, jQuery, undefined);
