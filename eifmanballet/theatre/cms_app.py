# coding=utf-8
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


@apphook_pool.register
class EnsembleApp(CMSApp):
    name = _("Ensemble App")
    urls = ["theatre.urls.ensemble"]
    app_name = 'ensemble'


@apphook_pool.register
class ScheduleApp(CMSApp):
    name = _("Schedule App")
    urls = ["theatre.urls.schedule"]
    app_name = 'schedule'


@apphook_pool.register
class RepertoireApp(CMSApp):
    name = _("Repertoire App")
    urls = ["theatre.urls.repertoire"]
    app_name = 'repertoire'


@apphook_pool.register
class VacancyApp(CMSApp):
    name = _('Vacancy app')
    urls = ['theatre.urls.vacancy']
    app_name = 'vacancy'
