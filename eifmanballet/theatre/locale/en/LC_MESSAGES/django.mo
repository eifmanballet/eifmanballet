��             +         �  �  �     b	     p	  	   }	  	   �	     �	     �	     �	     �	     �	  
   �	  \   �	  %   1
  <   W
  
   �
     �
     �
     �
  #   �
       (        @     Q  (   b  %   �     �     �     �     �     �  F   
  ?  Q  E  �     �     �  	   �  	   �               $     -     3     B     K     T     c     e     k     s     �     �     �     �     �     �     �     �     �  
   �     �     
                        	                                                                                                              
                    
                                В соответствии с Федеральным законом от 27.07.2006 г. № 152-ФЗ «О персональных данных»
                                подтверждаю свое согласие на передачу, обработку (включая сбор, систематизацию, накопление,
                                хранение, уточнение (обновление, изменение), использование, обезличивание, блокирование,
                                уничтожение персональных данных) СПб ГБУК «Академический театр Балета Бориса Эйфмана» (оператор)
                                моих персональных данных в целях содействия в трудоустройстве. Настоящее согласие дано
                                в отношении всех сведений, указанных в отправляемой Анкете. Условием прекращения обработки
                                персональных данных является получение оператором письменного уведомления об отзыве согласия
                                на обработку персональных данных. Настоящее согласие действует в течение 5 лет со дня его
                                получения оператором.
                             Corpsdeballet Ensemble App Page link Principal Repertoire App Schedule App Solist Tales Upcoming shows Афиша Афиша | новый театр русского балета Бориса Эйфмана Ближайшие спектакли В этом месяце пока что ничего нет Видео Галерея Заказ билетов Кордебалет Краткое содержание О спектакле Педагоги и репетиторы Площадка Площадки Подробнее о спектакле Премьера состоялась Пресса Репертуар Слово Б.Эйфмана Солисты Труппа Труппа русского балета Бориса Эйфмана Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-07-16 22:45+0300
PO-Revision-Date: 2017-07-16 23:07+0300
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: Oleg Tikhonov <oleg.t@mgk78.ru>
Language-Team: 
X-Generator: Poedit 2.0.2
 
In accordance with the Federal Law of the Russian Federation of 27 July 2006 №152-FZ “On Personal Data”, 
I thereby confirm my consent to the transfer, processing (including collecting, organizing, accumulating,
storing, clarifying (updating, changing), using, depersonalizing, blocking, 
destroying personal data) by “Boris Eifman Ballet Academy Theatre”, St. Petersburg State Budgetary Institution of Culture, (hereafter – Operator) 
of my personal data in the furtherance of employment. This consent applies 
to all the information specified in the Application. The condition for the termination of the processing of 
personal data is the receipt by Operator of a written notice of revocation of the consent 
to the processing of personal data. This consent is valid for 5 years from the date of its 
receipt by Operator. Corps de Ballet Ensemble App Page link Principal Repertoire App Schedule App Soloists Tales Upcoming shows Schedule Schedule Upcoming shows   Video Gallery Ticket purchasing Corps de Ballet Synopsis Resume Teachers and Tutors Place Places About the production Date of premiere Press Repertoire Introduction Soloists Ensemble Ensemble 