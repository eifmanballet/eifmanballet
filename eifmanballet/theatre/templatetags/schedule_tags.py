# coding=utf-8
from datetime import datetime
from itertools import groupby
from django import template
from theatre.models import ScheduleItem

register = template.Library()


@register.inclusion_tag('tags/map.html')
def schedule_map(year=None):
    now = datetime.now()
    if year is None:
        year = now.year
    objects = ScheduleItem.objects.filter(date__year=year, date__gte=now).order_by("date")
    finished_objects = ScheduleItem.objects.filter(date__year=year, date__lt=now).order_by("-date")
    objects = list(objects) + list(finished_objects)
    map_data = []

    for k, group in groupby(objects, lambda x: x.place):
        map_data.append(group.next())

    return {
        "objects": map_data,
    }
