# coding=utf-8
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import ListView, DetailView, MonthArchiveView, TemplateView
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from menus.utils import set_language_changer

from theatre.models import Person, Ballet, ScheduleItem
from theatre.forms import VacancyForm


def home(request):
    return render(request, 'home.html')


def ensemble_index(request):
    persons = Person.published\
        .filter(role__in=("solist", "tutor", "corpsdeballet")).order_by('role', 'last_name')

    return render(request, "theatre/ensemble_index.html", {
        "solists": persons.filter(role='solist'),
        "tutors": persons.filter(role='tutor').order_by('gender'),
        "corpsdeballet": persons.filter(role='corpsdeballet').order_by('gender', 'last_name'),
    })


class PersonView(DetailView):
    model = Person
    template_name = "theatre/person.html"


class BalletsList(ListView):
    model = Ballet
    template_name = "theatre/ballet_list.html"


class BalletView(DetailView):
    model = Ballet
    template_name = "theatre/ballet.html"

    # def get(self, request, *args, **kwargs):
    #     self.object = self.get_object()
    #     menu = request.toolbar.get_or_create_menu('ballet-app', _('Ballet'), position=2)
    #
    #     submenu = menu.get_or_create_menu('ballet-schedule', _('Schedule'))
    #     url = reverse('admin:theatre_scheduleitem_add')
    #     submenu.add_modal_item(_('Add show'), url='{}?ballet_id={}'.format(url, self.object.pk))
    #     if self.object.has_schedule:
    #         url = reverse('admin:theatre_scheduleitem_changelist')
    #         submenu.add_modal_item(_('Change'), url=url)
    #
    #     menu.add_break()
    #     url = reverse('admin:theatre_ballet_change', args=[self.object.pk, ])
    #     menu.add_modal_item(_('Edit this ballet'), url=url)
    #     url = reverse('admin:theatre_ballet_delete', args=[self.object.pk, ])
    #     menu.add_modal_item(_('Delete this ballet'), url=url)
    #     context = self.get_context_data(object=self.object)
    #     return self.render_to_response(context)


class ScheduleMixin(object):
    model = ScheduleItem
    date_field = 'date'
    make_object_list = True
    allow_empty = True
    allow_future = True
    month_format = "%m"

"""
    Метод не нужен, пока модель ScheduleItem не многоязычная
    def get_queryset(self):
        queryset = super(ScheduleMixin, self).get_queryset()
        if queryset:
            set_language_changer(self.request, queryset[0].language_changer)
        return queryset
"""


class ScheduleMonthView(ScheduleMixin, MonthArchiveView):
    template_name = 'theatre/schedule.html'


class ScheduleItemView(DetailView):
    model = ScheduleItem
    template_name = 'theatre/schedule_item_map.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        response = self.render_to_response(context)
        response['Access-Control-Allow-Origin'] = "*"
        return response


class VacancyFormView(FormView):
    form_class = VacancyForm
    template_name = 'theatre/vacancy.html'
    success_url = reverse_lazy('vacancy:success')

    def post(self, request, *args, **kwargs):
        if 'recaptcha_challenge_field' in request.POST and 'recaptcha_response_field' in request.POST:
            # This is a recaptcha protected form
            data = request.POST.copy()
            data['recaptcha_remote_ip_field'] = request.META['REMOTE_ADDR']
            request.POST = data
        return super(VacancyFormView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        form.send_email()
        return super(VacancyFormView, self).form_valid(form)


class VacancySuccessView(TemplateView):
    template_name = 'theatre/vacancy_success.html'
