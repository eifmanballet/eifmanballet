# coding=utf-8
from theatre.models import Person

person = Person.objects.create()
person.gender = "f"
person.role = "solist"
person.translate("ru")
person.first_name = "Мария"
person.last_name = "Абашова"
person.save()

person = Person.objects.create()
person.gender = "f"
person.role = "solist"
person.translate("ru")
person.first_name = "Любовь"
person.last_name = "Андреева"
person.save()

person = Person.objects.create()
person.gender = "m"
person.role = "solist"
person.translate("ru")
person.first_name = "Сергей"
person.last_name = "Волобуев"
person.save()

person = Person.objects.create()
person.gender = "m"
person.role = "solist"
person.translate("ru")
person.first_name = "Олег"
person.last_name = "Габышев"
person.save()

person = Person.objects.create()
person.gender = "f"
person.role = "tutor"
person.translate("ru")
person.first_name = "Валентина"
person.participant_name = "Николаевна"
person.last_name = "Морозова"
person.save()

person = Person.objects.create()
person.gender = "m"
person.role = "tutor"
person.translate("ru")
person.first_name = "Валерий"
person.participant_name = "Владимирович"
person.last_name = "Михайловский"
person.save()

person = Person.objects.create()
person.gender = "m"
person.role = "corps_de_ballet"
person.translate("ru")
person.first_name = "Анатолий"
person.last_name = "Грудзинский"
person.save()

person = Person.objects.create()
person.gender = "f"
person.role = "corps_de_ballet"
person.translate("ru")
person.first_name = "Евгения"
person.last_name = "Арутюнян"
person.save()
