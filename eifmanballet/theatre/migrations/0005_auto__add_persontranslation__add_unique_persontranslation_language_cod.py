# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PersonTranslation'
        db.create_table(u'theatre_person_translation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('participant_name', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('about_text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('language_code', self.gf('django.db.models.fields.CharField')(max_length=15, db_index=True)),
            ('master', self.gf('django.db.models.fields.related.ForeignKey')(related_name='translations', null=True, to=orm['theatre.Person'])),
        ))
        db.send_create_signal(u'theatre', ['PersonTranslation'])

        # Adding unique constraint on 'PersonTranslation', fields ['language_code', 'master']
        db.create_unique(u'theatre_person_translation', ['language_code', 'master_id'])

        # Deleting field 'Person.last_name'
        db.delete_column(u'theatre_person', 'last_name')

        # Deleting field 'Person.first_name'
        db.delete_column(u'theatre_person', 'first_name')

        # Deleting field 'Person.about_text'
        db.delete_column(u'theatre_person', 'about_text')

        # Deleting field 'Person.participant_name'
        db.delete_column(u'theatre_person', 'participant_name')

        # Deleting field 'Person.position'
        db.delete_column(u'theatre_person', 'position')

        # Adding field 'Person.created'
        db.add_column(u'theatre_person', 'created',
                      self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now),
                      keep_default=False)

        # Adding field 'Person.modified'
        db.add_column(u'theatre_person', 'modified',
                      self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now),
                      keep_default=False)


    def backwards(self, orm):
        # Removing unique constraint on 'PersonTranslation', fields ['language_code', 'master']
        db.delete_unique(u'theatre_person_translation', ['language_code', 'master_id'])

        # Deleting model 'PersonTranslation'
        db.delete_table(u'theatre_person_translation')

        # Adding field 'Person.last_name'
        db.add_column(u'theatre_person', 'last_name',
                      self.gf('django.db.models.fields.CharField')(default='Nobody', max_length=64),
                      keep_default=False)

        # Adding field 'Person.first_name'
        db.add_column(u'theatre_person', 'first_name',
                      self.gf('django.db.models.fields.CharField')(default='Nobody', max_length=64),
                      keep_default=False)

        # Adding field 'Person.about_text'
        db.add_column(u'theatre_person', 'about_text',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Person.participant_name'
        db.add_column(u'theatre_person', 'participant_name',
                      self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Person.position'
        db.add_column(u'theatre_person', 'position',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Person.created'
        db.delete_column(u'theatre_person', 'created')

        # Deleting field 'Person.modified'
        db.delete_column(u'theatre_person', 'modified')


    models = {
        u'theatre.person': {
            'Meta': {'object_name': 'Person'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'theatre.persontranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'PersonTranslation', 'db_table': "u'theatre_person_translation'"},
            'about_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': u"orm['theatre.Person']"}),
            'participant_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['theatre']