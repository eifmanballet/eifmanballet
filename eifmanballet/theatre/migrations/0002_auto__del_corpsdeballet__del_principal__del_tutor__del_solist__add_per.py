# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'CorpsDeBallet'
        db.delete_table(u'theatre_corpsdeballet')

        # Deleting model 'Principal'
        db.delete_table(u'theatre_principal')

        # Deleting model 'Tutor'
        db.delete_table(u'theatre_tutor')

        # Deleting model 'Solist'
        db.delete_table(u'theatre_solist')

        # Adding model 'Person'
        db.create_table(u'theatre_person', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('participant_name', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('about_text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'theatre', ['Person'])


    def backwards(self, orm):
        # Adding model 'CorpsDeBallet'
        db.create_table(u'theatre_corpsdeballet', (
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('participant_name', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'theatre', ['CorpsDeBallet'])

        # Adding model 'Principal'
        db.create_table(u'theatre_principal', (
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('participant_name', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=256)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'theatre', ['Principal'])

        # Adding model 'Tutor'
        db.create_table(u'theatre_tutor', (
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('participant_name', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
        ))
        db.send_create_signal(u'theatre', ['Tutor'])

        # Adding model 'Solist'
        db.create_table(u'theatre_solist', (
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('about_text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('participant_name', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('birth_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=2)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'theatre', ['Solist'])

        # Deleting model 'Person'
        db.delete_table(u'theatre_person')


    models = {
        u'theatre.person': {
            'Meta': {'object_name': 'Person'},
            'about_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'participant_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['theatre']