# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PressFeedbackTranslation'
        db.create_table(u'theatre_pressfeedback_translation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('publisher', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('pub_date', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('language_code', self.gf('django.db.models.fields.CharField')(max_length=15, db_index=True)),
            ('master', self.gf('django.db.models.fields.related.ForeignKey')(related_name='translations', null=True, to=orm['theatre.PressFeedback'])),
        ))
        db.send_create_signal(u'theatre', ['PressFeedbackTranslation'])

        # Adding unique constraint on 'PressFeedbackTranslation', fields ['language_code', 'master']
        db.create_unique(u'theatre_pressfeedback_translation', ['language_code', 'master_id'])

        # Adding model 'Ballet'
        db.create_table(u'theatre_ballet', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('cover', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'theatre', ['Ballet'])

        # Adding model 'BalletPhotoTranslation'
        db.create_table(u'theatre_balletphoto_translation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('language_code', self.gf('django.db.models.fields.CharField')(max_length=15, db_index=True)),
            ('master', self.gf('django.db.models.fields.related.ForeignKey')(related_name='translations', null=True, to=orm['theatre.BalletPhoto'])),
        ))
        db.send_create_signal(u'theatre', ['BalletPhotoTranslation'])

        # Adding unique constraint on 'BalletPhotoTranslation', fields ['language_code', 'master']
        db.create_unique(u'theatre_balletphoto_translation', ['language_code', 'master_id'])

        # Adding model 'BalletVideo'
        db.create_table(u'theatre_balletvideo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('video_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('ballet', self.gf('django.db.models.fields.related.ForeignKey')(related_name='videos', to=orm['theatre.Ballet'])),
        ))
        db.send_create_signal(u'theatre', ['BalletVideo'])

        # Adding model 'PressFeedback'
        db.create_table(u'theatre_pressfeedback', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('ballet', self.gf('django.db.models.fields.related.ForeignKey')(related_name='feedbacks', to=orm['theatre.Ballet'])),
        ))
        db.send_create_signal(u'theatre', ['PressFeedback'])

        # Adding model 'BalletTranslation'
        db.create_table(u'theatre_ballet_translation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('short_title', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('choreography', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('music', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('artist', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('costumes', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('decorations', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('light', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('premiere_date_textual', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('summary', self.gf('django.db.models.fields.TextField')()),
            ('eifmans_text', self.gf('django.db.models.fields.TextField')()),
            ('libretto', self.gf('django.db.models.fields.TextField')()),
            ('language_code', self.gf('django.db.models.fields.CharField')(max_length=15, db_index=True)),
            ('master', self.gf('django.db.models.fields.related.ForeignKey')(related_name='translations', null=True, to=orm['theatre.Ballet'])),
        ))
        db.send_create_signal(u'theatre', ['BalletTranslation'])

        # Adding unique constraint on 'BalletTranslation', fields ['language_code', 'master']
        db.create_unique(u'theatre_ballet_translation', ['language_code', 'master_id'])

        # Adding model 'BalletPhoto'
        db.create_table(u'theatre_balletphoto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('ballet', self.gf('django.db.models.fields.related.ForeignKey')(related_name='photos', to=orm['theatre.Ballet'])),
        ))
        db.send_create_signal(u'theatre', ['BalletPhoto'])


    def backwards(self, orm):
        # Removing unique constraint on 'BalletTranslation', fields ['language_code', 'master']
        db.delete_unique(u'theatre_ballet_translation', ['language_code', 'master_id'])

        # Removing unique constraint on 'BalletPhotoTranslation', fields ['language_code', 'master']
        db.delete_unique(u'theatre_balletphoto_translation', ['language_code', 'master_id'])

        # Removing unique constraint on 'PressFeedbackTranslation', fields ['language_code', 'master']
        db.delete_unique(u'theatre_pressfeedback_translation', ['language_code', 'master_id'])

        # Deleting model 'PressFeedbackTranslation'
        db.delete_table(u'theatre_pressfeedback_translation')

        # Deleting model 'Ballet'
        db.delete_table(u'theatre_ballet')

        # Deleting model 'BalletPhotoTranslation'
        db.delete_table(u'theatre_balletphoto_translation')

        # Deleting model 'BalletVideo'
        db.delete_table(u'theatre_balletvideo')

        # Deleting model 'PressFeedback'
        db.delete_table(u'theatre_pressfeedback')

        # Deleting model 'BalletTranslation'
        db.delete_table(u'theatre_ballet_translation')

        # Deleting model 'BalletPhoto'
        db.delete_table(u'theatre_balletphoto')


    models = {
        u'theatre.ballet': {
            'Meta': {'object_name': 'Ballet'},
            'cover': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'})
        },
        u'theatre.balletphoto': {
            'Meta': {'object_name': 'BalletPhoto'},
            'ballet': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photos'", 'to': u"orm['theatre.Ballet']"}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'})
        },
        u'theatre.balletphototranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'BalletPhotoTranslation', 'db_table': "u'theatre_balletphoto_translation'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': u"orm['theatre.BalletPhoto']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'theatre.ballettranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'BalletTranslation', 'db_table': "u'theatre_ballet_translation'"},
            'artist': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'choreography': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'costumes': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'decorations': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'eifmans_text': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'libretto': ('django.db.models.fields.TextField', [], {}),
            'light': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': u"orm['theatre.Ballet']"}),
            'music': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'premiere_date_textual': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'short_title': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'summary': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'theatre.balletvideo': {
            'Meta': {'object_name': 'BalletVideo'},
            'ballet': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'videos'", 'to': u"orm['theatre.Ballet']"}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'video_url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'theatre.person': {
            'Meta': {'object_name': 'Person'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'theatre.personphoto': {
            'Meta': {'object_name': 'PersonPhoto'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'person': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photos'", 'to': u"orm['theatre.Person']"})
        },
        u'theatre.persontranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'PersonTranslation', 'db_table': "u'theatre_person_translation'"},
            'about_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': u"orm['theatre.Person']"}),
            'participant_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        u'theatre.pressfeedback': {
            'Meta': {'object_name': 'PressFeedback'},
            'ballet': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'feedbacks'", 'to': u"orm['theatre.Ballet']"}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'})
        },
        u'theatre.pressfeedbacktranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'PressFeedbackTranslation', 'db_table': "u'theatre_pressfeedback_translation'"},
            'author': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': u"orm['theatre.PressFeedback']"}),
            'pub_date': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'publisher': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['theatre']