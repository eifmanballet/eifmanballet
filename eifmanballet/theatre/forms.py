# coding=utf-8
from __future__ import unicode_literals

from django import forms
from django.utils.translation import get_language, ugettext_lazy as _
from django.core.mail import EmailMessage
from django.conf import settings
from django.template import loader

from recaptcha_works import settings as recaptcha_settings
from recaptcha_works.fields import RecaptchaField

from theatre.models import Person


class PrincipalPluginForm(forms.ModelForm):
    person = forms.ModelChoiceField(
        queryset=Person.objects.filter(role='principial')
    )


CONTENT_TYPES = ['image', 'video']
SIZE_1MB = 1024 * 1024
# 2.5MB - 2621440
# 5MB - 5242880
# 10MB - 10485760
# 20MB - 20971520
# 50MB - 5242880
# 100MB 104857600
# 250MB - 214958080
# 500MB - 429916160
MAX_UPLOAD_SIZE = SIZE_1MB * 15
MAX_UPLOAD_SIZE_HUMAN = '15MB'


class VacancyForm(forms.Form):
    fio = forms.CharField(label=_('Last name, First name, Middle name'), max_length=255, required=True,
                          widget=forms.TextInput(attrs={
                              'class': 'form__input',
                              'placeholder': _('Your full name'),
                              'type': 'text',
                          }))
    birth_date = forms.CharField(label=_('Your birth date'), required=True,
                                 widget=forms.TextInput(attrs={
                                     'class': 'form__input',
                                     'placeholder': _('Your birth date'),
                                     'type': 'text',
                                 }))
    phone = forms.CharField(label=_('Your phone number'), max_length=32, required=True,
                            widget=forms.TextInput(attrs={
                                'class': 'form__input',
                                'placeholder': _('Your phone number'),
                                'type': 'text',
                            }))
    email = forms.EmailField(label=_('Your email'), required=True,
                             widget=forms.EmailInput(attrs={
                                 'class': 'form__input',
                                 'placeholder': _('Your email'),
                                 'type': 'email',
                             }))
    achivements = forms.CharField(label=_('Your achivements'), required=True,
                                  widget=forms.Textarea(attrs={
                                      'class': 'form__textarea',
                                  }))
    place_of_study = forms.CharField(label=_('Places of study'), required=True,
                                     widget=forms.Textarea(attrs={
                                         'class': 'form__textarea',
                                     }))
    place_of_work = forms.CharField(label=_('Places of work'), required=True,
                                    widget=forms.Textarea(attrs={
                                        'class': 'form__textarea',
                                    }))
    personal = forms.CharField(label=_('Growth, weight'), required=True,
                               widget=forms.Textarea(attrs={
                                   'class': 'form__textarea',
                               }))
    links = forms.CharField(label=_('Links to your video, profile with photos'), required=False,
                            widget=forms.Textarea(attrs={
                                'class': 'form__textarea',
                            }))
    resume = forms.FileField(label=_('Resume'), required=False,
                             help_text=_('Max file size is %(file_size)s.') % {'file_size': MAX_UPLOAD_SIZE_HUMAN},
                             widget=forms.FileInput(attrs={
                                 'class': 'form__file',
                             }))
    terms_of_use = forms.BooleanField(label='', required=True,
                                      widget=forms.CheckboxInput(attrs={
                                          'class': 'form__checkbox',
                                      }))
    recaptcha = RecaptchaField(label=_('Human test'),
                               recaptcha_options=getattr(recaptcha_settings, 'RECAPTCHA_OPTIONS', {}).update({
                                   'lang': get_language(),
                               }),
                               required=True)

    @staticmethod
    def _clean_nl2br(text):
        if text:
            return text.replace('\n', '<br />')
        return text

    def clean_resume(self):
        data = self.cleaned_data
        resume = data.get('resume')
        if resume and resume.size > 0 and resume.size > MAX_UPLOAD_SIZE:
            raise forms.ValidationError(_('Too large file size. Max size is %(file_size)s.') % {
                'file_size': MAX_UPLOAD_SIZE_HUMAN
            })
        return resume

    def clean_links(self):
        return self._clean_nl2br(self.cleaned_data.get('links'))

    def clean_personal(self):
        return self._clean_nl2br(self.cleaned_data.get('personal'))

    def clean_place_of_study(self):
        return self._clean_nl2br(self.cleaned_data.get('place_of_study'))

    def clean_place_of_work(self):
        return self._clean_nl2br(self.cleaned_data.get('place_of_work'))

    def clean_achivements(self):
        return self._clean_nl2br(self.cleaned_data.get('achivements'))

    def send_email(self):
        email = EmailMessage(
            'Вакансия с сайта %(site)s' % {'site': settings.EMAIL_SUBJECT_PREFIX},
            loader.render_to_string('theatre/mail/vacancy.html', {'form': self}),
            settings.DEFAULT_SENDER,
            settings.VACANCIES_CONTACT_EMAILS
        )
        email.content_subtype = 'html'
        if self.cleaned_data.get('resume') and self.cleaned_data.get('resume').size > 0:
            resume = self.cleaned_data.get('resume')
            email.attach(resume.name, resume.read(), resume.content_type)
        try:
            email.send(fail_silently=False)
        except Exception as e:
            raise e

