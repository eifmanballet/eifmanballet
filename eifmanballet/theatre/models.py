# coding=utf-8
from cms.models import CMSPlugin
from cms.models.fields import PageField
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField as HTMLField
from filer.fields.image import FilerImageField
from hvad.manager import TranslationManager
from hvad.models import TranslatableModel, TranslatedFields
from model_utils.models import TimeStampedModel
from youtubeurl_field.modelfields import YoutubeUrlField
from geo.models import City

GENDER_CHOICES = (
    ("m", _("Male"),),
    ("f", _("Female"),),
)

PERSON_ROLES = (
    ("solist", _("Solist")),
    ("tutor", _("Tutor")),
    ("corpsdeballet", _("Corpsdeballet")),
    ("principial", _("Principial")),
)

NETWORKS_CHOICES = (
    ('vk', _('vkontakte')),
    ('fb', _('facebook')),
    ('tw', _('twitter')),
    ('insta', _('instagram')),
    ('youtube', _('youtube')),
)


class PublishedManager(TranslationManager):
    def get_queryset(self):
        return self.language().filter(is_published=True)


class Person(TranslatableModel, TimeStampedModel):
    translations = TranslatedFields(
        first_name=models.CharField(_("First Name"), max_length=64),
        participant_name=models.CharField(_("Participant Name"), max_length=64, null=True, blank=True),
        last_name=models.CharField(_("Last Name"), max_length=64),
        position=models.CharField(_("Position"), max_length=256, null=True, blank=True),
        about_text=HTMLField(_("About person"), null=True, blank=True),
    )
    photo = FilerImageField(verbose_name=_("Photo"), null=True, blank=True)
    gender = models.CharField(_("Gender"), choices=GENDER_CHOICES, max_length=2)
    role = models.CharField(_("Role"), choices=PERSON_ROLES, max_length=32)
    is_published = models.BooleanField(_("Is published"), default=True)

    published = PublishedManager()

    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")

    @models.permalink
    def get_absolute_url(self):
        return ("person_page", (), {
            "role": self.role,
            "pk": self.pk
        })

    def is_artist(self):
        return self.role in ('solist', 'corpsdeballet',)

    @property
    def has_additional_info(self):
        return self.photos.count() > 0 or self.safe_translation_getter("about_text") is not None

    @property
    def full_name(self):
        f_name = self.lazy_translation_getter("first_name")
        l_name = self.lazy_translation_getter("last_name")
        m_name = self.lazy_translation_getter("participant_name")
        if self.is_artist():
            return u"{} {}".format(f_name, l_name)
        else:
            return u"{} {} {}".format(l_name, f_name, m_name)

    @property
    def seo_title(self):
        try:
            return self.seo_fields.lazy_translation_getter('title')
        except:
            return self.full_name

    @property
    def seo_description(self):
        return self.seo_fields.lazy_translation_getter('description')

    def __unicode__(self):
        return self.full_name


class PersonPhoto(TimeStampedModel):
    image = FilerImageField(verbose_name=_("Photo"))
    person = models.ForeignKey(Person, verbose_name=_("Person"), related_name="photos")
    weight = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        verbose_name = _("Person photo")
        verbose_name_plural = _("Person photos")
        ordering = ('weight', )

    def __unicode__(self):
        return self.person.full_name


class Ballet(TranslatableModel, TimeStampedModel):
    translations = TranslatedFields(
        title=models.CharField(_("Title"), max_length=255),
        titers=HTMLField(_("Titers"), default=""),
        summary=HTMLField(_("Summary")),
        eifmans_text=HTMLField(_("Eifmans text")),
        libretto=HTMLField(_("Libretto"), blank=True, null=True),
    )
    slug = models.SlugField(_("Slug"), max_length=255, db_index=True, unique=True)
    cover = FilerImageField(verbose_name=_("Cover image"), related_name='cover', null=True, blank=True)
    schedule_image = FilerImageField(verbose_name=_("Image for schedule"), related_name='schedule_cover',
                                     null=True, blank=True, help_text=_('If not presented, cover image will used'))
    is_published = models.BooleanField(_("Is published"), default=True)
    weight = models.PositiveIntegerField(default=0, blank=False, null=False)

    published = PublishedManager()

    class Meta:
        verbose_name = _("Ballet")
        verbose_name_plural = _("Ballets")
        ordering = ('weight', )

    @property
    def scheduled_items(self):
        return self.schedule.filter(date__gte=now())

    @property
    def has_video(self):
        return any([x.video_url.value for x in self.videos.all()])

    @property
    def has_gallery(self):
        return self.photos.count() > 0

    @property
    def has_schedule(self):
        return self.scheduled_items.count() > 0

    @property
    def has_feedback(self):
        return self.feedbacks.language(self.language_code).count() > 0

    @property
    def seo_title(self):
        try:
            return self.seo_fields.lazy_translation_getter('title')
        except:
            return self.lazy_translation_getter('title')

    @property
    def seo_description(self):
        return self.seo_fields.lazy_translation_getter('description')

    def __unicode__(self):
        return self.lazy_translation_getter('title')

    @models.permalink
    def get_absolute_url(self):
        return ("ballet_page", (), {
            "slug": self.slug
        })


class BalletSeo(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(_('title'), max_length=255, null=True, blank=True),
        description=models.TextField(_('description'), null=True, blank=True),
    )
    ballet = models.OneToOneField(Ballet, related_name='seo_fields')

    class Meta:
        verbose_name = _("Seo Fields")
        verbose_name_plural = _("Seo Fields")


class PersonSeo(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(_('title'), max_length=255, null=True, blank=True),
        description=models.TextField(_('description'), null=True, blank=True),
    )
    person = models.OneToOneField(Person, related_name='seo_fields')

    class Meta:
        verbose_name = _("Seo Fields")
        verbose_name_plural = _("Seo Fields")


class BalletPhoto(TranslatableModel, TimeStampedModel):
    translations = TranslatedFields(
        title=models.CharField(_("Title"), max_length=255, null=True, blank=True),
    )
    image = FilerImageField(verbose_name=_("Image"))
    ballet = models.ForeignKey(Ballet, verbose_name="Ballet", related_name="photos")
    weight = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        verbose_name = _("Ballet photo")
        verbose_name_plural = _("Ballet photos")
        ordering = ('weight', )

    def __unicode__(self):
        title = self.lazy_translation_getter('title')
        if title == "":
            title = self.image.label
        return title


class BalletVideo(TimeStampedModel):
    video_url = YoutubeUrlField(_("Video URL"), null=False, blank=False, max_length=255)
    ballet = models.ForeignKey(Ballet, verbose_name="Ballet", related_name="videos")
    weight = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        verbose_name = _("Ballet video")
        verbose_name_plural = _("Ballet videos")
        ordering = ('weight', )

    def __unicode__(self):
        return self.ballet.title


class PressFeedback(TranslatableModel, TimeStampedModel):
    translations = TranslatedFields(
        author=models.CharField(_("Author"), max_length=255, null=True, blank=True),
        title=models.CharField(_("Title"), max_length=255, null=True, blank=True),
        publisher=models.CharField(_("Publisher"), max_length=255, null=True, blank=True),
        pub_date=models.CharField(_("Publication date"), max_length=255, null=True, blank=True),
        text=models.TextField(_("Text"), null=True, blank=True)
    )
    ballet = models.ForeignKey(Ballet, verbose_name=_("Ballet"), related_name="feedbacks")
    weight = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        verbose_name = _("Press feedback")
        verbose_name_plural = _("Press feedbacks")
        ordering = ('weight', )

    def __unicode__(self):
        title = self.safe_translation_getter('title')
        publisher = self.safe_translation_getter('publisher')
        return u"{} ({})".format(title if title is not None else publisher, self.ballet.title)


class Place(TranslatableModel, TimeStampedModel):
    translations = TranslatedFields(
        title=models.CharField(_("Title"), max_length=255)
    )
    city = models.ForeignKey(City, verbose_name=_("City"))

    class Meta:
        verbose_name = _(u'Площадка')
        verbose_name_plural = _(u'Площадки')

    def __unicode__(self):
        return self.lazy_translation_getter('title')


class ScheduleItem(TranslatableModel, TimeStampedModel):
    translations = TranslatedFields(
        custom_title=models.CharField(_("Custom title"), max_length=255, null=True, blank=True),
    )
    cover = FilerImageField(verbose_name=_("cover image"), null=True, blank=True)
    ballet = models.ForeignKey(Ballet, verbose_name=_("Ballet"),
                               related_name="schedule", null=True, blank=True)
    date = models.DateField(_("Date"))
    time = models.TimeField(_("Time"))
    place = models.ForeignKey(Place, verbose_name=_("Place"), null=True, blank=True)
    city = models.ForeignKey(City, verbose_name=_("City"), null=True, blank=True,
                             help_text=_('Use when place is unknown, usually with custom title. Overrides place'))
    booking_link = models.URLField(_("Booking link"), null=True, blank=True)

    class Meta:
        verbose_name = _("Show")
        verbose_name_plural = _("Shows")
        ordering = ['date', 'time', ]

    @property
    def item_cover(self):
        if self.cover is not None:
            return self.cover
        if self.ballet is not None:
            return self.ballet.schedule_image or self.ballet.cover
        return None

    @property
    def admin_city(self):
        return self.city or self.place.city

    @property
    def title(self):
        custom_title = self.lazy_translation_getter('custom_title')
        ballet_title = self.ballet.lazy_translation_getter('title') if self.ballet else None
        if ballet_title and custom_title:
            return u"{}<br>{}".format(ballet_title, custom_title)
        if ballet_title:
            return u"{}".format(self.ballet.lazy_translation_getter('title'))
        else:
            return self.lazy_translation_getter('custom_title')

    def clean(self):
        if self.place is None and self.city is None:
            raise ValidationError(_("Place or City must be filled"))
        if self.place is not None and self.city is not None:
            raise ValidationError(_("Select City OR Place"))

    def __unicode__(self):
        return u"{} {} в {}".format(
            self.title,
            self.date,
            self.time,
        )


class UpcomingShowsPlugin(CMSPlugin):
    limit = models.PositiveIntegerField(_('Number of shows to show'),
                                        help_text=_('Limits the number of shows that will be displayed'))


class SocialNetworks(models.Model):
    type = models.CharField(_("Type"), choices=NETWORKS_CHOICES, max_length=16)
    url = models.URLField(_("URL"))
    weight = models.PositiveIntegerField(default=0, blank=False, null=False)

    @property
    def img_path(self):
        return 'i/{}-logo.svg'.format(self.type)

    class Meta:
        ordering = ['weight', ]
        verbose_name = _("Social Network")
        verbose_name_plural = _("Social Network")

    def __unicode__(self):
        return dict(NETWORKS_CHOICES)[self.type]


POSITION_CHOICES = (
    ('left', _('left')),
    ('right', _('right')),
)


class Tales(TranslatableModel, TimeStampedModel):
    translations = TranslatedFields(
        text=HTMLField(_("Text")),
        signature=FilerImageField(verbose_name=_("signature"), null=True, blank=True),
    )
    author = models.ForeignKey(Person, verbose_name="author")
    photo = FilerImageField(verbose_name=_("Photo"), null=True, blank=True)
    photo_position = models.CharField(_("Photo position"), choices=POSITION_CHOICES, max_length=8)
    weight = models.IntegerField(_("Weight"))
    is_published = models.BooleanField(_("Is Published"), default=True)

    published = PublishedManager()

    def __unicode__(self):
        return u"{} ({}...)".format(
            self.author,
            self.lazy_translation_getter('text')[:40]
        )

    class Meta:
        ordering = ['weight', ]
        verbose_name = _("Tale")
        verbose_name_plural = _("Tales")


class TalesPlugin(CMSPlugin):
    limit = models.PositiveIntegerField(_('Number of tales to show'),
                                        help_text=_('Limits the number of tales that will be displayed'))


class PageLinkPlugin(CMSPlugin):
    page_link = PageField(null=True, blank=True, help_text=_("if present image will be clickable"),
                          verbose_name=_("page link"))
    image = FilerImageField(blank=True, null=True, verbose_name=_("image"))

    def __unicode__(self):
        return self.page_link.get_menu_title()


class PrincipalPlugin(CMSPlugin):
    person = models.ForeignKey(Person, verbose_name=_("person"))
    show_photo = models.BooleanField(_("show photo"), default=False)

    def __unicode__(self):
        return self.person.full_name
