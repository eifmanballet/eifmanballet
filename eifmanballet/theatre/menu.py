# coding=utf-8
from cms.menu_bases import CMSAttachMenu
from django.core.urlresolvers import reverse
from menus.base import Menu, NavigationNode
from menus.menu_pool import menu_pool
from django.utils.translation import ugettext_lazy as _
from theatre.models import Ballet


@menu_pool.register_menu
class BalletMenu(CMSAttachMenu):
    name = _("Ballets menu")

    def get_nodes(self, request):
        nodes = []
        for ballet in Ballet.objects.all():
            nodes.append(NavigationNode(ballet.title,
                         reverse('repertoire:ballet_page', kwargs={'slug': ballet.slug}),
                         ballet.pk))
        return nodes
