# coding=utf-8
from datetime import datetime
from django.utils.translation import ugettext_lazy as _
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase
from theatre.forms import PrincipalPluginForm
from theatre.models import UpcomingShowsPlugin, ScheduleItem, TalesPlugin, Tales, PageLinkPlugin, PrincipalPlugin

@plugin_pool.register_plugin
class CMSUpcomingShows(CMSPluginBase):
    model = UpcomingShowsPlugin
    name = _("Upcoming shows")
    render_template = "theatre/plugins/upcoming_shows.html"

    def render(self, context, instance, placeholder):
        today = datetime.today()
        qs = ScheduleItem.objects.filter(date__gte=today)[:instance.limit]

        context.update({
            'instance': instance,
            'items': qs,
            'object_list': qs,
            'placeholder': placeholder,
        })

        return context


@plugin_pool.register_plugin
class CMSTales(CMSPluginBase):
    model = TalesPlugin
    name = _("Tales")
    render_template = "theatre/plugins/tales.html"

    def render(self, context, instance, placeholder):
        qs = Tales.published.order_by('weight')[:instance.limit]

        context.update({
            'instance': instance,
            'items': qs,
            'object_list': qs,
            'placeholder': placeholder,
        })

        return context


@plugin_pool.register_plugin
class CMSPageLink(CMSPluginBase):
    model = PageLinkPlugin
    module = 'Theatre'
    name = _("Page link")
    raw_id_fields = ('image', 'page_link')
    render_template = "theatre/plugins/page_link.html"

    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance,
            'placeholder': placeholder,
            'object': instance
        })
        return context


@plugin_pool.register_plugin
class CMSPrincipal(CMSPluginBase):
    model = PrincipalPlugin
    form = PrincipalPluginForm
    module = 'Theatre'
    name = _("Principal")
    render_template = "theatre/plugins/principal.html"

    def render(self, context, instance, placeholder):
        context.update({
            'show_photo': instance.show_photo and instance.person.photo,
            'person': instance.person,
            'instance': instance,
            'placeholder': placeholder,
            'object': instance
        })
        return context
