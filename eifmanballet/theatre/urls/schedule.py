# coding=utf-8
from datetime import datetime
from django.conf.urls import patterns, url
from theatre.views import ScheduleMonthView, ScheduleItemView

year = datetime.today().year
month = datetime.today().month

cur_month_kwargs = {
    "year": str(year),
    "month": str(month),
}

urlpatterns = patterns("schedule",
   url(r"^$", ScheduleMonthView.as_view(), cur_month_kwargs, name='schedule_index'),
   url(r"^(?P<year>\d{4})$", ScheduleMonthView.as_view(), {"month": "1"}, name="month_view"),
   url(r"^(?P<year>\d{4})/(?P<month>\d{1,2})$", ScheduleMonthView.as_view(), name="month_view"),
   url(r"^map/$", lambda x: x, name="api_root"),
   url(r"^map/(?P<pk>\d+)$", ScheduleItemView.as_view(), name="schedule_item")
   )
