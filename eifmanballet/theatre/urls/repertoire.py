# coding=utf-8
from django.conf.urls import patterns, url
from theatre.views import BalletsList, BalletView


urlpatterns = patterns("eifmanballet.theatre.views",
   url(r"^$", BalletsList.as_view(), name="repertoire_index"),
   url(r"^(?P<slug>[-a-zA-Z0-9_]+)$", BalletView.as_view(), name="ballet_page"),
   )
