# coding=utf-8
from django.conf.urls import patterns, url
from django.views.generic import RedirectView
from theatre.views import ensemble_index, PersonView


urlpatterns = patterns("theatre",
   url(r"^$", ensemble_index, name="ensemble_index"),
   # TODO: try to remove hardcoded string
   url(r"^(?P<role>[a-z]+)$", RedirectView.as_view(url="/ensemble")),
   url(r"^(?P<role>[a-z]+)/(?P<pk>\d+)$", PersonView.as_view(), name="person_page"),
   )
