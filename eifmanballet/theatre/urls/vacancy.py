# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from django.conf.urls import patterns, url
from theatre import views

__author__ = 'traffaret'


urlpatterns = patterns('',
                       url(r'^$', views.VacancyFormView.as_view(), name='form'),
                       url(r'^success/$', views.VacancySuccessView.as_view(), name='success'),
                       )
