from adminsortable.admin import SortableInlineAdminMixin
from django.contrib import admin
from hvad.admin import TranslatableAdmin, TranslatableStackedInline
from theatre.models import Person, PersonPhoto, PersonSeo


class PersonPhotoInline(SortableInlineAdminMixin, admin.TabularInline):
    model = PersonPhoto
    extra = 0

class PersonSeoInline(TranslatableStackedInline):
    model = PersonSeo
    extra = 0


class PersonAdmin(TranslatableAdmin):
    list_display = (
        'full_name',
        'role',
        'is_published',
        'created',
    )
    list_filter = (
        'role',
        'is_published',
    )
    fields = (
        'last_name',
        'first_name',
        'participant_name',
        'photo',
        'role',
        'gender',
        'position',
        'about_text',
        'is_published',
    )
    inlines = [
        PersonPhotoInline,
        PersonSeoInline,
    ]

admin.site.register(Person, PersonAdmin)
