# coding=utf-8
from adminsortable.admin import SortableAdminMixin
from django.contrib import admin
from theatre.models import SocialNetworks


class SocialNetworksAdmin(SortableAdminMixin, admin.ModelAdmin):
    pass

admin.site.register(SocialNetworks, SocialNetworksAdmin)
