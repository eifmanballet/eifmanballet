# coding=utf-8 
from adminsortable.admin import SortableAdminMixin, SortableInlineAdminMixin
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from hvad.admin import TranslatableAdmin, TranslatableStackedInline
from theatre.models import Ballet, BalletPhoto, BalletVideo, PressFeedback, BalletSeo


class BalletPhotoInline(SortableInlineAdminMixin, admin.TabularInline):
    model = BalletPhoto
    extra = 0


class BalletVideoInline(SortableInlineAdminMixin, admin.TabularInline):
    model = BalletVideo
    extra = 0


class BalletFeedbackInline(TranslatableStackedInline):
    model = PressFeedback
    extra = 0


class BalletSeoInline(TranslatableStackedInline):
    model = BalletSeo
    extra = 0


class BalletAdmin(SortableAdminMixin, TranslatableAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'title',
                'slug',
                'cover',
                'schedule_image',
                'titers',
                'is_published',
            ),
        }),
        (_('Text in tabs'), {
            'fields': (
                'summary',
                'eifmans_text',
                'libretto',
            ),
            'classes': ('collapse',),
        })
    )
    inlines = [
        BalletPhotoInline,
        BalletVideoInline,
        BalletFeedbackInline,
        BalletSeoInline,
    ]

admin.site.register(Ballet, BalletAdmin)
