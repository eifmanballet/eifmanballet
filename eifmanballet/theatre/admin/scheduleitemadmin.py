# coding=utf-8
from django.contrib import admin
from hvad.admin import TranslatableAdmin
from theatre.models import ScheduleItem


class BalletRelationsAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'ballet':
            ballet_id = request.GET.get('ballet_id')
            kwargs['initial'] = ballet_id
        return super(BalletRelationsAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class ScheduleItemAdmin(BalletRelationsAdmin, TranslatableAdmin):
    list_display = (
        'title',
        'date',
        'time',
        'admin_city',
        'place',
        'created',
    )
    list_filter = (
        'ballet',
    )
    fieldsets = (
        (None, {
            "fields": (
                'ballet',
                'custom_title',
                'cover',
                'booking_link',
            ),
        }),
        (None, {
            "fields": (
                'date',
                'time',
                'place',
                'city',
            ),
        }),
    )

admin.site.register(ScheduleItem, ScheduleItemAdmin)
