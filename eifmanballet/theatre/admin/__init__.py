import theatre.admin.balletadmin
import theatre.admin.personadmin
import theatre.admin.scheduleitemadmin
import theatre.admin.socialnetworksadmin
from django.contrib import admin
from hvad.admin import TranslatableAdmin
from theatre.models import Place, Tales

admin.site.register(Place, TranslatableAdmin)
admin.site.register(Tales, TranslatableAdmin)
