# coding=utf-8
import json
import urllib
import urllib2
import xml.etree.ElementTree as ET
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from hvad.models import TranslatableModel, TranslatedFields
from geo import utils


class Country(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(_("title"), max_length=255)
    )
    default_country = models.BooleanField(_("Default country"), default=False)

    def __unicode__(self):
        return self.lazy_translation_getter('title', '')


class City(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(_("title"), max_length=255)
    )
    country = models.ForeignKey(Country, verbose_name=_("Country"))
    latitude = models.FloatField(_("Latitude"), blank=True, null=True)
    longitude = models.FloatField(_("Longitude"), blank=True, null=True)

    def __unicode__(self):
        return u"{} ({})".format(*self._get_geocode_data())

    def _get_geocode_data(self):
        return self.lazy_translation_getter('title'), self.country.__unicode__()

    def save(self, *args, **kwargs):
        if self.latitude is None and self.longitude is None:
            geo_name = u"{} {}".format(*self._get_geocode_data()).encode('utf-8')
            coords = utils.get_coords_by_name(geo_name)

            if coords is not None:
                self.latitude, self.longitude = coords

        super(City, self).save(*args, **kwargs)
