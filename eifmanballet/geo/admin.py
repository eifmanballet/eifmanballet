# coding=utf-8 
from django.contrib import admin
from hvad.admin import TranslatableAdmin
from .models import Country, City

admin.site.register(Country, TranslatableAdmin)
admin.site.register(City, TranslatableAdmin)
