# coding=utf-8
import urllib
import urllib2
import xml.etree.ElementTree as ET
from django.conf import settings


def get_coords_by_name(name):
    query_data = urllib.urlencode({
        "geocode": name,
        "format": "xml",
    })

    request = urllib2.Request(settings.GEOCODE_URL, query_data)
    response = urllib2.urlopen(request)
    response_data = response.read()
    data = ET.fromstring(response_data)

    coords = data.find('.//{http://www.opengis.net/gml}pos')

    if coords is not None:
        return coords.text.split(" ")
