# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CountryTranslation'
        db.create_table(u'geo_country_translation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('language_code', self.gf('django.db.models.fields.CharField')(max_length=15, db_index=True)),
            ('master', self.gf('django.db.models.fields.related.ForeignKey')(related_name='translations', null=True, to=orm['geo.Country'])),
        ))
        db.send_create_signal(u'geo', ['CountryTranslation'])

        # Adding unique constraint on 'CountryTranslation', fields ['language_code', 'master']
        db.create_unique(u'geo_country_translation', ['language_code', 'master_id'])

        # Adding model 'Country'
        db.create_table(u'geo_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'geo', ['Country'])

        # Adding model 'CityTranslation'
        db.create_table(u'geo_city_translation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('language_code', self.gf('django.db.models.fields.CharField')(max_length=15, db_index=True)),
            ('master', self.gf('django.db.models.fields.related.ForeignKey')(related_name='translations', null=True, to=orm['geo.City'])),
        ))
        db.send_create_signal(u'geo', ['CityTranslation'])

        # Adding unique constraint on 'CityTranslation', fields ['language_code', 'master']
        db.create_unique(u'geo_city_translation', ['language_code', 'master_id'])

        # Adding model 'City'
        db.create_table(u'geo_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['geo.Country'])),
        ))
        db.send_create_signal(u'geo', ['City'])


    def backwards(self, orm):
        # Removing unique constraint on 'CityTranslation', fields ['language_code', 'master']
        db.delete_unique(u'geo_city_translation', ['language_code', 'master_id'])

        # Removing unique constraint on 'CountryTranslation', fields ['language_code', 'master']
        db.delete_unique(u'geo_country_translation', ['language_code', 'master_id'])

        # Deleting model 'CountryTranslation'
        db.delete_table(u'geo_country_translation')

        # Deleting model 'Country'
        db.delete_table(u'geo_country')

        # Deleting model 'CityTranslation'
        db.delete_table(u'geo_city_translation')

        # Deleting model 'City'
        db.delete_table(u'geo_city')


    models = {
        u'geo.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['geo.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'geo.citytranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'CityTranslation', 'db_table': "u'geo_city_translation'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': u"orm['geo.City']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'geo.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'geo.countrytranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'CountryTranslation', 'db_table': "u'geo_country_translation'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': u"orm['geo.Country']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['geo']