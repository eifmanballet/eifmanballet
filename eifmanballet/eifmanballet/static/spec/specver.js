$(function() {
    var spec_button = $('#spec');

	function loadform(url) {
		/* $( "<button style='margin-top: 50px; float:right'  onclick='$.removeCookie(\"openspec\", { path: \"/\" });location.reload();'>Закрыть версию для слабовидящих</button>" ).insertBefore( "body" ); */
		$("body").before( "<iframe width='100%' height='51'  style='border: none;' src='" + url + "'></iframe><button class='spec-escape' onclick='$.removeCookie(\"openspec\", { path: \"/\" });location.reload();'>Закрыть версию для слабовидящих</button>" );
	}

	spec_button.click(function(e) {
		if (!$.cookie('openspec')){
			$.cookie('openspec', 1, { path: '/' });
			$('body *').css("font-size: 22px");
			$('img').css("-webkit-filter: grayscale(100%)");
			$('body *').css("line-height: 1.5");
			loadform($(this).data('url'));
		}
	});
	if ($.cookie('openspec') == 1) {
        loadform(spec_button.data('url'));
    }
});