$(function() {
    function loadstyle() {
        if ($.cookie('font-size'))
            parent.$("body *").css("font-size", $.cookie('font-size'));
        if ($.cookie('background'))
            parent.$("body, body *").css("background", $.cookie('background'));
        if ($.cookie('color'))
            parent.$("body *").css("color", $.cookie('color'));
        if ($.cookie('letter-spacing'))
            parent.$("body *").css("letter-spacing", $.cookie('letter-spacing'));
		if ($.cookie('img') == '1')
			parent.$("body img").show();
		if ($.cookie('img') == '2')
			parent.$("body img").css("filter", "grayscale(100%)");
		if ($.cookie('img') == '3')
			parent.$("body img").hide();
		if ($.cookie('line-height'))
			parent.$("body *").css("line-height", $.cookie('line-height'));
    }
	loadstyle();
    $("#a1").click(function() {
        parent.$('body *').css("font-size", "18px");
        $.cookie('font-size', parent.$("body *").css("font-size"));
    });
    $("#a2").click(function() {
        parent.$('body *').css("font-size", "22px");
        $.cookie('font-size', parent.$("body *").css("font-size"));
    });
	$("#a3").click(function() {
        parent.$('body *').css("font-size", "26px");
        $.cookie('font-size', parent.$("body *").css("font-size"));
    });
	$("#a4").click(function() {
        parent.$('body *').css("font-size", "30px");
        $.cookie('font-size', parent.$("body *").css("font-size"));
    });
	$("#a5").click(function() {
        parent.$('body *').css("font-size", "34px");
        $.cookie('font-size', parent.$("body *").css("font-size"));
    });
    $("#c1").click(function() {
        parent.$("body, body *").css("background", "#fff");
        parent.$("body *").css("color", "#000");
        $.cookie('background', "#fff");
        $.cookie('color', "#000");
    });
    $("#c2").click(function() {
        parent.$("body, body *").css("background", "#000");
        parent.$("body *").css("color", "#fff");
        $.cookie('background', "#000");
        $.cookie('color', "#fff");
    });
    $("#c3").click(function() {
        parent.$("body, body *").css("background", "#6699FF");
        parent.$("body *").css("color", "#fff");
        $.cookie('background', "#6699FF");
        $.cookie('color', "#fff");
    });
    $("#c4").click(function() {
        parent.$("body, body *").css("background", "#f7f3d6");
        parent.$("body *").css("color", "#4d4b43");
        $.cookie('background', "#f7f3d6");
        $.cookie('color', "#4d4b43");
    });
    $("#c5").click(function() {
        parent.$("body, body *").css("background", "#3b2716");
        parent.$("body *").css("color", "#a9e44d");
        $.cookie('background', "#3b2716");
        $.cookie('color', "#a9e44d");
    });
	$("#v1").click(function() {
		parent.$("body img").show();
		parent.$("img").css("filter", "");
		$.cookie('img', '1');
	});
	$("#v2").click(function() {
		parent.$("body img").show();
		parent.$("img").css("filter", "grayscale(100%)");
		$.cookie('img', '2');
	});
	$("#v3").click(function() {
		parent.$("body img").hide();
		$.cookie('img', '3');
	});
	$("#i1").click(function() {
		var $spacing = $.cookie('letter-spacing');
		$spacing = parseInt($spacing);
		if ($spacing > 0){
			$spacing = $spacing - 1;
			parent.$("body *").css({'letter-spacing': $spacing});
			$.cookie('letter-spacing', parent.$("body *").css("letter-spacing"));
		}
    });
    $("#i2").click(function() {
		var $spacing = $.cookie('letter-spacing');
		$spacing = parseInt($spacing);
		if ($spacing < 10)
		$spacing = $spacing + 1;
        parent.$("body *").css({'letter-spacing': $spacing});
        $.cookie('letter-spacing', parent.$("body *").css("letter-spacing"));
    });
	$("#tr").change(function(){
		var $tracking = $("select#tr").val();
		switch ($tracking) {
			case '1':
				parent.$("body *").css("line-height", "1");
				break;
			case '2':
				parent.$("body *").css("line-height", "1.25");
				break;
			case '3':
				parent.$("body *").css("line-height", "1.5");
				break;
			case '4':
				parent.$("body *").css("line-height", "1.75");
				break;
			case '5':
				parent.$("body *").css("line-height", "2");
				break;
			case '6':
				parent.$("body *").css("line-height", "2.5");
				break;
			case '7':
				parent.$("body *").css("line-height", "3");
				break;
		}
		$.cookie('line-height', parent.$("body *").css("line-height"));
	});
    $("#reset").click(function() {
        $.cookie('line-height', '');
        $.cookie('font-size', '');
        $.cookie('letter-spacing', '');
        $.cookie('font-family', '');
        $.cookie('img', '');
        $.cookie('color', '');
        $.cookie('background', '');
        parent.location.reload();
    });
});
