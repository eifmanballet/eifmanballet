# coding = utf-8

from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

SOUTH_MIGRATION_MODULES = {
    'easy_thumbnails': 'easy_thumbnails.south_migrations',
}

CONTACT_EMAILS = [
    'oleg.t@mgk78.ru',
]

ADMINS = (
    ('Oleg Tikhonov', 'oleg.t@mgk78.ru',)
)

ALLOWED_HOSTS = [
    '.eifmanballet.ru',
    '.eifmanballet.com',
]

STATIC_URL = '/htdocs/eifmanballet/static/'
MEDIA_URL = '/htdocs/eifmanballet/media/'

BASE_URL = 'http://new.eifmanballet.ru'
