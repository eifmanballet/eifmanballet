# coding = utf-8

from .base import *

INSTALLED_APPS = INSTALLED_APPS + (
    'debug_toolbar',
)

SOUTH_MIGRATION_MODULES = {
    'easy_thumbnails': 'easy_thumbnails.south_migrations',
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# INTERNAL_IPS = ["127.0.0.1", ]
