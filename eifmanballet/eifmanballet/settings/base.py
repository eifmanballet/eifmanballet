# coding=utf-8
"""
Django settings for eifmanballet project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.utils.translation import ugettext_lazy as _
gettext = lambda s: s

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'djangocms_admin_style',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'south',
    'cms',
    'hvad',
    'mptt',
    'menus',
    'sekizai',
    'compressor',
    'djangocms_admin_style',
    'adminsortable',
    'djangocms_text_ckeditor',
    'djangocms_link',
    'djangocms_snippet',
    'djangocms_grid',
    'djangocms_column',
    'djangocms_snippet',
    'filer',
    'easy_thumbnails',
    'cmsplugin_filer_file',
    'cmsplugin_filer_folder',
    'cmsplugin_filer_image',
    'cmsplugin_filer_teaser',
    'cmsplugin_filer_video',
    'cmsplugin_filer_video',
    'reversion',
    'ckeditor',
    'recaptcha_works',

    'cmsplugin_eifman_teaser',
    'cmsplugin_eifman_gallery',
    'cmsplugin_eifman_contentbox',
    'cmsplugin_eifman_youtube',

    'google_analytics',
    'contacts',
    'theatre',
    'news',
    'geo',
    'promo',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'middleware.locale.PatchedLocaleMiddleware',
    'django.middleware.common.CommonMiddleware',

    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
    'django.core.context_processors.debug',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'cms.context_processors.cms_settings',
    'sekizai.context_processors.sekizai',
    'utils.context_processors.baseurl',
)

ROOT_URLCONF = 'eifmanballet.urls'

WSGI_APPLICATION = 'eifmanballet.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '../databases/db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('ru', gettext('Russian')),
    ('en', gettext('English')),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

CMS_LANGUAGES = {
    'default': {
        'hide_untranslated': False,
        'redirect_on_fallback': True,
        'public': True,
    },
    1: [
        {
            'redirect_on_fallback': True,
            'code': 'ru',
            'hide_untranslated': False,
            'name': gettext('ru'),
            'public': True,
        },
        {
            'redirect_on_fallback': True,
            'code': 'en',
            'hide_untranslated': False,
            'name': gettext('en'),
            'public': True,
        },
    ],
}


TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

STATIC_ROOT = COMPRESS_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = '/static/'
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
]
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'eifmanballet/static'),
]

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = '/media/'

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar_Full': [
            ['Undo', 'Redo', '-', 'ShowBlocks'],
            ['Format', 'Styles'],
            ['TextColor', 'BGColor', '-', 'PasteText', 'PasteFromWord'],
            ['Maximize', ''],
            '/',
            ['Bold', 'Italic', 'Underline', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink', "Image", "Anchor", "Youtube"],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Table'],
            ['Source']
        ],
    }
}

CKEDITOR_SETTINGS = {
    'language': '{{ language }}',
    'toolbar_CMS': [
        ['Undo', 'Redo'],
        ['cmsplugins', '-', 'ShowBlocks'],
        ['Format', 'Styles'],
        ['TextColor', 'BGColor', '-', 'PasteText', 'PasteFromWord'],
        ['Maximize', ''],
        '/',
        ['Bold', 'Italic', 'Underline', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Link', 'Unlink', "Image", "Anchor", "Youtube"],
        ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Table'],
        ['Source']
    ],
    'toolbar': 'CMS',
    'skin': 'moono',
#    'stylesSet': [
#        {'name': 'Custom Style', 'element': 'h3', 'styles': {'color': 'Blue'}}
#    ],
    'toolbarCanCollapse': False,
}

CKEDITOR_UPLOAD_PATH = 'uploads/'

CKEDITOR_IMAGE_BACKEND = 'pillow'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

APPEND_SLASH = False

CMS_TEMPLATES = (
    ('index.html', _('Index Page')),
    ('page.html', _('Common Page')),
    ('page_2.html', _('Common Page, menu with line')),
    ('theatre.html', _('Theatre page')),
    ('theatre-eifman.html', _('Eifman\'s page')),
    ('theatre-inner.html', _('Theatre inner page')),
    ('theatre-partners.html', _('Partners page')),
    ('spec.html', _('Spec page')),
)

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    # 'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

GEOCODE_URL = "http://geocode-maps.yandex.ru/1.x/"

CONTACT_EMAILS = [
    "none@none.ru",
]
VACANCIES_CONTACT_EMAILS = CONTACT_EMAILS

EMAIL_SUBJECT_PREFIX = "[eifmanballet.ru] "

DEFAULT_SENDER = "no-reply@eifmanballet.ru"

RECAPTCHA_PUBLIC_KEY  = '6LdELvYSAAAAAIYBKqUqjpCZmz427W-QBeKmK2e2'

RECAPTCHA_PRIVATE_KEY = '6LdELvYSAAAAAJkWqGmodwmP0BvaqrMXK8UmtNVj'

RECAPTCHA_USE_SSL = False

RECAPTCHA_OPTIONS = {
    'theme': 'white',
}

COMPRESS_CSS_FILTERS = [
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter',
]

COMPRESS_JS_FILTERS = [
    'compressor.filters.jsmin.JSMinFilter',
]