# coding = utf-8
import os
from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG
COMPRESS_ENABLED = False

SOUTH_MIGRATION_MODULES = {
    'easy_thumbnails': 'easy_thumbnails.south_migrations',
}

CONTACT_EMAILS = [
    'pr@eifmanballet.ru',
]
VACANCIES_CONTACT_EMAILS = [
    'vacancies@eifmanballet.com',
]

ADMINS = (
    ('Mikhail Baranov', 'dev@brnv.ru',)
)

ALLOWED_HOSTS = [
    '.eifmanballet.ru',
    '.eifmanballet.com',
]

STATIC_URL = '/htdocs/eifmanballet/static/'
MEDIA_URL = '/htdocs/eifmanballet/media/'

BASE_URL = 'http://www.eifmanballet.ru'

ANALYTICS_ID = 'UA-52085199-1'
ANALYTICS_SITENAME = 'eifmanballet.ru'

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
) + MIDDLEWARE_CLASSES + (
    'django.middleware.cache.FetchFromCacheMiddleware',
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/tmp/eifmanballet_cache',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = 'mail.valuehost.ru'
EMAIL_PORT = 25
EMAIL_USE_TLS = False
EMAIL_USE_SSL = False
EMAIL_HOST_USER = 'mail@eifmanballet.ru'
EMAIL_HOST_PASSWORD = os.environ.get('DJANGO_EMAIL_HOST_PASSWORD')
