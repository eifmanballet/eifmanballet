# coding = utf-8

from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

INTERNAL_IPS = [
    "127.0.0.1",
    "94.19.215.21",
    "178.130.45.219",
    ]
