# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin
from youtubeurl_field.modelfields import YoutubeUrlField


class EifmanYoutubePlugin(CMSPlugin):
    url = YoutubeUrlField('url', max_length=255)
    width = models.PositiveIntegerField(_('width'), default=640)
    height = models.PositiveIntegerField(_('height'), default=480)

    def __unicode__(self):
        return self.url.embed_url