# coding=utf-8
import os

from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cmsplugin_eifman_youtube.models import EifmanYoutubePlugin


@plugin_pool.register_plugin
class CMSEifmanYoutube(CMSPluginBase):
    model = EifmanYoutubePlugin
    module = 'Theatre'
    name = _("Youtube")
    raw_id_fields = ('image', 'page_link')
    render_template = "cmsplugin_eifman_youtube/youtube.html"
    text_enabled = True

    def icon_src(self, instance):
        return instance.url.thumb

    def render(self, context, instance, placeholder):
        context.update({
            'object': instance,
            'placeholder': placeholder,
        })
        return context
