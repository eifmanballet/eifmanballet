# coding=utf-8
from django.conf import settings


def baseurl(request):
    """
    Adds static-related context variables to the context.

    """
    request_host = '{}://{}'.format('https' if request.is_secure() else 'http', request.get_host())

    return {'BASE_URL': getattr(settings, 'BASE_URL', request_host)}
