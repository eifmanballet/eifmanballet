# coding=utf-8
from cmsplugin_filer_image.models import ThumbnailOption
from django.utils.translation import ugettext_lazy as _
from django.db import models
from cms.models import CMSPlugin
from filer.fields.folder import FilerFolderField

SHOW_ARROWS = (
    ('always', _("Always")),
    ('when_need', _("When need")),
)


class EifmanGalleryPlugin(CMSPlugin):
    title = models.CharField(_("title"), max_length=255, null=True, blank=True)
    folder = FilerFolderField()
    class_name = models.CharField(_("Block class name"), max_length=255, blank=True, null=True)
    thumbnail_option = models.ForeignKey(ThumbnailOption, null=True, blank=True, verbose_name=_("thumbnail option"),
                                        help_text=_('overrides width, height, crop and upscale with values from the selected thumbnail option'))
    width = models.PositiveIntegerField(_("Item width"))
    height = models.PositiveIntegerField(_("Item height"))
    crop = models.BooleanField(_("Crop"), default=False)
    show_arrows = models.CharField(_("Show arrows"), max_length="64", choices=SHOW_ARROWS, default='when_need')

    def __unicode__(self):
        if self.title:
            return self.title
        elif self.folder.name:
            return self.folder.name
        return "<empty>"
