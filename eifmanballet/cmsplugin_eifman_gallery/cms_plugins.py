# coding=utf-8
import json
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _
from cmsplugin_eifman_gallery.models import EifmanGalleryPlugin
from filer.models.imagemodels import Image


@plugin_pool.register_plugin
class EifmanGalleryPlugin(CMSPluginBase):
    module = "Theatre"
    name = _("Gallery")
    model = EifmanGalleryPlugin
    text_enabled = False
    render_template = "cmsplugin_eifman_gallery/gallery.html"

    @staticmethod
    def get_folder_images(folder, user):
        qs_files = folder.files.instance_of(Image)
        if user.is_staff:
            return qs_files
        else:
            return qs_files.filter(is_public=True)

    @staticmethod
    def _get_thumbnail_options(context, instance):
        width, height = None, None

        if instance.thumbnail_option:
            if instance.thumbnail_option.width:
                width = instance.thumbnail_option.width
            if instance.thumbnail_option.height:
                height = instance.thumbnail_option.height
            crop = instance.thumbnail_option.crop
        else:
            width = instance.width
            height = instance.height
            crop = instance.crop

        return {
            'size': (width, height),
            'width': width,
            'height': height,
            'crop': crop,
        }

    @staticmethod
    def _get_script_params(instance):
        params = {
            'show_arrows': instance.show_arrows
        }
        return json.dumps(params)

    def render(self, context, instance, placeholder):
        options = self._get_thumbnail_options(context, instance)
        folder_images = self.get_folder_images(instance.folder,
                                               context['request'].user)
        js_params = self._get_script_params(instance)

        context.update({
            'object': instance,
            'folder_images': sorted(folder_images),
            'size': options.get('size',None),
            'placeholder': placeholder,
            'opts': options,
            'js_params': js_params
        })
        return context
