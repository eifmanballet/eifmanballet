# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin
from cms.models.fields import PageField
from cmsplugin_filer_image.models import ThumbnailOption
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.image import FilerImageField


class EifmanTeaserPlugin(CMSPlugin):
    title = models.CharField(_("Title"), max_length=255)
    page_link = PageField(null=True, blank=True, verbose_name=_("page link"))
    image = FilerImageField(blank=True, null=True, verbose_name=_("image"))
    thumbnail_option = models.ForeignKey(ThumbnailOption, null=True, blank=True, verbose_name=_("thumbnail option"),
                                        help_text=_('overrides width, height, crop and upscale with values from the selected thumbnail option'))
    width = models.PositiveIntegerField(_("width"), null=True, blank=True)
    height = models.PositiveIntegerField(_("height"), null=True, blank=True)
    crop = models.BooleanField(_("Crop"), default=False)

    class_name = models.CharField(_("Block ClassName"), blank=True, max_length=255, default="")
    teaser_text = HTMLField(_("Text"))

    def __unicode__(self):
        return self.title
