# coding=utf-8
from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cmsplugin_eifman_teaser.models import EifmanTeaserPlugin


@plugin_pool.register_plugin
class CMSEifmanTeaser(CMSPluginBase):
    model = EifmanTeaserPlugin
    module = 'Theatre'
    name = _("Teaser")
    raw_id_fields = ('image', 'page_link')
    render_template = "cmsplugin_eifman_teaser/teaser.html"

    def _get_thumbnail_options(self, context, instance):
        width, height = None, None
        placeholder_width = context.get('width', None)
        placeholder_height = context.get('height', None)
        subject_location = None

        if instance.thumbnail_option:
            # thumbnail option overrides everything else
            if instance.thumbnail_option.width:
                width = instance.thumbnail_option.width
            if instance.thumbnail_option.height:
                height = instance.thumbnail_option.height
            crop = instance.thumbnail_option.crop
        else:
            if instance.use_autoscale and placeholder_width:
                # use the placeholder width as a hint for sizing
                width = int(placeholder_width)
            elif instance.width:
                width = instance.width
            if instance.use_autoscale and placeholder_height:
                height = int(placeholder_height)
            elif instance.height:
                height = instance.height
            crop = instance.crop

        if instance.image:
            if instance.image.subject_location:
                subject_location = instance.image.subject_location
            if not height and width:
                # height was not externally defined: use ratio to scale it by the width
                height = int( float(width)*float(instance.image.height)/float(instance.image.width) )
            if not width and height:
                # width was not externally defined: use ratio to scale it by the height
                width = int( float(height)*float(instance.image.width)/float(instance.image.height) )
            if not width:
                # width is still not defined. fallback the actual image width
                width = instance.image.width
            if not height:
                # height is still not defined. fallback the actual image height
                height = instance.image.height

        return {
            'size': (width, height),
            'crop': crop,
            'subject_location': subject_location
        }

    def render(self, context, instance, placeholder):
        options = self._get_thumbnail_options(context, instance)

        context.update({
            'instance': instance,
            'placeholder': placeholder,
            'opts': options,
            'size': options.get('size',None),
            'object': instance
        })
        return context
