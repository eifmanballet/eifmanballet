# coding=utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cmsplugin_eifman_contentbox.models import ContentBox
from django.utils.translation import ugettext_lazy as _


@plugin_pool.register_plugin
class ContentBoxPlugin(CMSPluginBase):
    model = ContentBox
    name = _("Content box")
    module = "Theatre"
    render_template = 'cmsplugin_eifman_contentbox/contentbox.html'
    allow_children = True
