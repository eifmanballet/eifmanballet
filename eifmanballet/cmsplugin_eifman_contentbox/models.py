# coding=utf-8
from django.db import models
from cms.models import CMSPlugin
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

BOX_TYPES_DEFAULTS = [
    ('belt', _("Belt")),
    ('regular', _("Regular")),
    ('medium', _("Medium")),
    ('narrow', _("Narrow")),
]
BOX_TYPES = getattr(settings, 'EIFMAN_BOX_TYPES', BOX_TYPES_DEFAULTS)


class ContentBox(CMSPlugin):
    type = models.CharField(_("Box type"), max_length=64, choices=BOX_TYPES)
    custom_classes = models.CharField(_("custom classes"), max_length=255, blank=True, null=True)
    translatable_content_excluded_fields = ['custom_classes', 'type']

    def __unicode__(self):
        return self.type
        # return getattr(dict(BOX_TYPES), self.type, _("<empty>"))
