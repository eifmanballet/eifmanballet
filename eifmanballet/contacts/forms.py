# coding=utf-8
from django.forms import ModelForm
from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from recaptcha_works import settings
from recaptcha_works.fields import RecaptchaField
from contacts.models import Feedback


class ContactsForm(ModelForm):
    opts = getattr(settings, 'RECAPTCHA_OPTIONS', {})
    opts['lang'] = translation.get_language()
    recaptcha = RecaptchaField(label=_('Human test'), recaptcha_options=opts, required=True)

    class Meta:
        model = Feedback
        exclude = ['ip_address']