from django.contrib import admin
from hvad.admin import TranslatableAdmin
from contacts.models import Feedback, Address

admin.site.register(Address, TranslatableAdmin)
admin.site.register(Feedback)
