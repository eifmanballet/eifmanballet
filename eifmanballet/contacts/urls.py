# coding=utf-8
from django.conf.urls import patterns, url
from contacts.views import ContactsView, ThanksView


urlpatterns = patterns("eifmanballet.contacts.views",
   url(r"^$", ContactsView.as_view(), name="contacts_index"),
   url(r"^thanks$", ThanksView.as_view(), name="thanks_page"),
   )
