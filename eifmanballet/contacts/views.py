# coding=utf-8
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView
from django.views.generic import TemplateView
from contacts.forms import ContactsForm


class ContactsView(CreateView):
    template_name = 'contacts/feedback_form.html'
    form_class = ContactsForm
    success_url = reverse_lazy('contacts:thanks_page')
    fields = [
        'name',
        'email',
        'phone',
        'subject',
        'message',
        ]

    def form_valid(self, form):
        form.instance.ip_address = self.request.META['REMOTE_ADDR']
        return super(ContactsView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        if 'recaptcha_challenge_field' in request.POST and 'recaptcha_response_field' in request.POST:
            # This is a recaptcha protected form
            data = request.POST.copy()
            data['recaptcha_remote_ip_field'] = request.META['REMOTE_ADDR']
            request.POST = data
        return super(ContactsView, self).post(request, *args, **kwargs)


class ThanksView(TemplateView):
    template_name = 'contacts/thanks.html'
