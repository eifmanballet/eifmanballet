# coding=utf-8 
from django import template
from sekizai.context import SekizaiContext
from contacts.models import Address

register = template.Library()


@register.inclusion_tag('tags/address.html',
                        context_class=SekizaiContext,
                        takes_context=True)
def address_tags(context):
    address = Address.objects.all()

    return {
        "objects": address
    }
