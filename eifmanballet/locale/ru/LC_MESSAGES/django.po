# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-03 10:43+0300\n"
"PO-Revision-Date: 2017-05-03 10:45+0300\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"Last-Translator: Oleg Tikhonov <oleg.t@mgk78.ru>\n"
"Language-Team: \n"
"X-Generator: Poedit 2.0.1\n"

#: cmsplugin_eifman_contentbox/cms_plugins.py:11
msgid "Content box"
msgstr ""

#: cmsplugin_eifman_contentbox/models.py:8
msgid "Belt"
msgstr ""

#: cmsplugin_eifman_contentbox/models.py:9
msgid "Regular"
msgstr ""

#: cmsplugin_eifman_contentbox/models.py:10
msgid "Medium"
msgstr ""

#: cmsplugin_eifman_contentbox/models.py:11
msgid "Narrow"
msgstr ""

#: cmsplugin_eifman_contentbox/models.py:17
msgid "Box type"
msgstr ""

#: cmsplugin_eifman_contentbox/models.py:18
msgid "custom classes"
msgstr ""

#: cmsplugin_eifman_gallery/cms_plugins.py:13
msgid "Gallery"
msgstr ""

#: cmsplugin_eifman_gallery/models.py:9
msgid "Always"
msgstr ""

#: cmsplugin_eifman_gallery/models.py:10
msgid "When need"
msgstr ""

#: cmsplugin_eifman_gallery/models.py:15 geo/models.py:15 geo/models.py:25
#: theatre/models.py:179 theatre/models.py:191
msgid "title"
msgstr ""

#: cmsplugin_eifman_gallery/models.py:17
msgid "Block class name"
msgstr ""

#: cmsplugin_eifman_gallery/models.py:18 cmsplugin_eifman_teaser/models.py:15
#: cmsplugin_filer_image/models.py:27 cmsplugin_filer_image/models.py:111
msgid "thumbnail option"
msgstr ""

#: cmsplugin_eifman_gallery/models.py:19 cmsplugin_eifman_teaser/models.py:16
#: cmsplugin_filer_image/models.py:28
msgid ""
"overrides width, height, crop and upscale with values from the selected "
"thumbnail option"
msgstr ""

#: cmsplugin_eifman_gallery/models.py:20
msgid "Item width"
msgstr ""

#: cmsplugin_eifman_gallery/models.py:21
msgid "Item height"
msgstr ""

#: cmsplugin_eifman_gallery/models.py:22 cmsplugin_eifman_teaser/models.py:19
msgid "Crop"
msgstr ""

#: cmsplugin_eifman_gallery/models.py:23
msgid "Show arrows"
msgstr ""

#: cmsplugin_eifman_teaser/cms_plugins.py:12
msgid "Teaser"
msgstr ""

#: cmsplugin_eifman_teaser/models.py:12 news/models.py:20 theatre/models.py:116
#: theatre/models.py:203 theatre/models.py:238 theatre/models.py:259
msgid "Title"
msgstr ""

#: cmsplugin_eifman_teaser/models.py:13 cmsplugin_filer_image/models.py:41
#: theatre/models.py:388
msgid "page link"
msgstr ""

#: cmsplugin_eifman_teaser/models.py:14 cmsplugin_filer_image/models.py:19
#: theatre/models.py:389
msgid "image"
msgstr ""

#: cmsplugin_eifman_teaser/models.py:17 cmsplugin_eifman_youtube/models.py:10
#: cmsplugin_filer_image/models.py:31 cmsplugin_filer_image/models.py:104
#: promo/models.py:17
msgid "width"
msgstr ""

#: cmsplugin_eifman_teaser/models.py:18 cmsplugin_eifman_youtube/models.py:11
#: cmsplugin_filer_image/models.py:32 cmsplugin_filer_image/models.py:105
#: promo/models.py:18
msgid "height"
msgstr ""

#: cmsplugin_eifman_teaser/models.py:21
msgid "Block ClassName"
msgstr ""

#: cmsplugin_eifman_teaser/models.py:22 theatre/models.py:241
#: theatre/models.py:358
msgid "Text"
msgstr ""

#: cmsplugin_eifman_youtube/cms_plugins.py:14
msgid "Youtube"
msgstr ""

#: cmsplugin_filer_image/cms_plugins.py:13 news/models.py:57
#: theatre/models.py:205
msgid "Image"
msgstr ""

#: cmsplugin_filer_image/cms_plugins.py:22
msgid "Image resizing options"
msgstr ""

#: cmsplugin_filer_image/cms_plugins.py:33
msgid "More"
msgstr ""

#: cmsplugin_filer_image/models.py:15 theatre/models.py:351
msgid "left"
msgstr ""

#: cmsplugin_filer_image/models.py:16 theatre/models.py:352
msgid "right"
msgstr ""

#: cmsplugin_filer_image/models.py:18
msgid "caption text"
msgstr ""

#: cmsplugin_filer_image/models.py:21 cmsplugin_filer_image/models.py:23
msgid "alternative image url"
msgstr ""

#: cmsplugin_filer_image/models.py:24
#, fuzzy
msgid "alt text"
msgstr "лалала"

#: cmsplugin_filer_image/models.py:25
msgid "use the original image"
msgstr ""

#: cmsplugin_filer_image/models.py:26
msgid "do not resize the image. use the original image instead."
msgstr ""

#: cmsplugin_filer_image/models.py:29
msgid "use automatic scaling"
msgstr ""

#: cmsplugin_filer_image/models.py:30
msgid "tries to auto scale the image based on the placeholder context"
msgstr ""

#: cmsplugin_filer_image/models.py:33 cmsplugin_filer_image/models.py:106
msgid "crop"
msgstr ""

#: cmsplugin_filer_image/models.py:34 cmsplugin_filer_image/models.py:107
msgid "upscale"
msgstr ""

#: cmsplugin_filer_image/models.py:35
msgid "image alignment"
msgstr ""

#: cmsplugin_filer_image/models.py:37
msgid "link"
msgstr ""

#: cmsplugin_filer_image/models.py:38 cmsplugin_filer_image/models.py:40
#: cmsplugin_filer_image/models.py:42 cmsplugin_filer_image/models.py:43
#: theatre/models.py:387
msgid "if present image will be clickable"
msgstr ""

#: cmsplugin_filer_image/models.py:42
msgid "file link"
msgstr ""

#: cmsplugin_filer_image/models.py:43
msgid "link original image"
msgstr ""

#: cmsplugin_filer_image/models.py:44 theatre/models.py:180
#: theatre/models.py:192
msgid "description"
msgstr ""

#: cmsplugin_filer_image/models.py:45
msgid "Open link in new window"
msgstr ""

#: cmsplugin_filer_image/models.py:54
msgid "filer image"
msgstr ""

#: cmsplugin_filer_image/models.py:55
msgid "filer images"
msgstr ""

#: cmsplugin_filer_image/models.py:61
msgid "Either an image or an image url must be selected."
msgstr ""

#: cmsplugin_filer_image/models.py:68
#, python-format
msgid "Image Publication %(caption)s"
msgstr ""

#: cmsplugin_filer_image/models.py:103 contacts/models.py:29
msgid "name"
msgstr ""

#: cmsplugin_filer_image/models.py:104
msgid "width in pixel."
msgstr ""

#: cmsplugin_filer_image/models.py:105
msgid "height in pixel."
msgstr ""

#: cmsplugin_filer_image/models.py:112
msgid "thumbnail options"
msgstr ""

#: contacts/cms_app.py:9
msgid "Contacts App"
msgstr ""

#: contacts/forms.py:13 theatre/forms.py:86
msgid "Human test"
msgstr ""

#: contacts/models.py:12 contacts/models.py:19
msgid "address"
msgstr ""

#: contacts/models.py:14
msgid "latitude"
msgstr ""

#: contacts/models.py:15
msgid "longitude"
msgstr ""

#: contacts/models.py:16
msgid "weight"
msgstr ""

#: contacts/models.py:20
msgid "addresses"
msgstr ""

#: contacts/models.py:30
msgid "email"
msgstr ""

#: contacts/models.py:31
msgid "phone"
msgstr ""

#: contacts/models.py:32
msgid "subject"
msgstr ""

#: contacts/models.py:33
msgid "message"
msgstr ""

#: contacts/models.py:34
msgid "ip address"
msgstr ""

#: contacts/models.py:37
msgid "feedback"
msgstr ""

#: contacts/models.py:38
msgid "feedbacks"
msgstr ""

#: contacts/templates/contacts/feedback_form.html:35
msgid "Обратная связь"
msgstr ""

#: contacts/templates/contacts/feedback_form.html:40
#: theatre/templates/theatre/vacancy.html:27
msgid "Submit"
msgstr ""

#: contacts/templates/contacts/thanks.html:30
msgid "Ваше сообщение отправлено. Спасибо!"
msgstr ""

#: eifmanballet/settings/base.py:135
msgid "Russian"
msgstr "Русский"

#: eifmanballet/settings/base.py:136
msgid "English"
msgstr "English"

#: eifmanballet/settings/base.py:154
msgid "ru"
msgstr ""

#: eifmanballet/settings/base.py:161
msgid "en"
msgstr ""

#: eifmanballet/settings/base.py:243
msgid "Index Page"
msgstr ""

#: eifmanballet/settings/base.py:244
msgid "Common Page"
msgstr ""

#: eifmanballet/settings/base.py:245
msgid "Common Page, menu with line"
msgstr ""

#: eifmanballet/settings/base.py:246
msgid "Theatre page"
msgstr ""

#: eifmanballet/settings/base.py:247
msgid "Eifman's page"
msgstr ""

#: eifmanballet/settings/base.py:248
msgid "Theatre inner page"
msgstr ""

#: eifmanballet/settings/base.py:249
msgid "Partners page"
msgstr ""

#: eifmanballet/settings/base.py:250
msgid "Spec page"
msgstr ""

#: geo/models.py:17
msgid "Default country"
msgstr ""

#: geo/models.py:27
msgid "Country"
msgstr ""

#: geo/models.py:28
msgid "Latitude"
msgstr ""

#: geo/models.py:29
msgid "Longitude"
msgstr ""

#: news/cms_app.py:8
msgid "News App"
msgstr ""

#: news/cms_plugins.py:17
msgid "Latest news"
msgstr ""

#: news/cms_toolbar.py:11 news/models.py:30
msgid "News"
msgstr ""

#: news/cms_toolbar.py:13
msgid "Add news"
msgstr ""

#: news/models.py:19
msgid "Lead"
msgstr ""

#: news/models.py:21
msgid "Description"
msgstr ""

#: news/models.py:24 theatre/models.py:53 theatre/models.py:126
msgid "Is published"
msgstr ""

#: news/models.py:25
msgid "Publication Date"
msgstr ""

#: news/models.py:31
msgid "News_plural"
msgstr ""

#: news/models.py:39
#, python-format
msgid "News at: %s"
msgstr ""

#: news/models.py:66
msgid "Number of news to show"
msgstr ""

#: news/models.py:67
msgid "Limits the number of news that will be displayed"
msgstr ""

#: news/templates/news/latest_news.html:3 news/templates/news/news_list.html:7
msgid "Recent news"
msgstr ""

#: news/templates/news/latest_news.html:12
#: news/templates/news/news_archive_day.html:15
#: news/templates/news/news_list.html:27
msgid "No news for this day"
msgstr ""

#: news/templates/news/latest_news.html:16
msgid "Все новости"
msgstr ""

#: news/templates/news/news_archive_day.html:5
#: news/templates/news/news_archive_month.html:5
#: news/templates/news/news_archive_year.html:5
msgid "News for"
msgstr ""

#: news/templates/news/news_archive_month.html:5
#: news/templates/news/news_archive_year.html:5
msgid "year"
msgstr ""

#: news/templates/news/news_archive_month.html:15
msgid "No news for this month"
msgstr ""

#: news/templates/news/news_archive_year.html:15
msgid "No news for this year"
msgstr ""

#: news/templates/news/news_item.html:4
#, fuzzy
msgid "театр балета Бориса Эйфмана"
msgstr ""
"Русский балет | Санкт-Петербургский новый, современный театр классического "
"балета в Александринском театре балета | официальный сайт театра русского "
"балета Бориса Эйфмана"

#: news/templates/news/news_item.html:5
msgid ""
"Санкт-Петербургский Государственный Академический Театр Балета Бориса "
"Эйфмана. "
msgstr ""

#: news/templates/news/news_item.html:26
msgid "Новости"
msgstr ""

#: promo/cms_plugins.py:10
msgid "Promo block"
msgstr ""

#: promo/models.py:14
msgid "Normal Image"
msgstr ""

#: promo/models.py:15
msgid "Blurred Image"
msgstr ""

#: promo/models.py:16 theatre/cms_toolbar.py:11 theatre/models.py:132
#: theatre/models.py:243 theatre/models.py:276
msgid "Ballet"
msgstr ""

#: promo/models.py:19
msgid "Handler coordinates"
msgstr ""

#: promo/models.py:20
msgid "Image coordinates"
msgstr ""

#: promo/models.py:21
msgid "Row number"
msgstr ""

#: promo/models.py:22
msgid "Position in row"
msgstr ""

#: promo/models.py:25 promo/models.py:65
msgid "Item"
msgstr ""

#: promo/models.py:26
msgid "Items"
msgstr ""

#: promo/models.py:60
msgid "text"
msgstr ""

#: promo/models.py:61
msgid "Color"
msgstr ""

#: promo/models.py:62
msgid "Font size"
msgstr ""

#: promo/models.py:63 theatre/models.py:47
msgid "Position"
msgstr ""

#: promo/models.py:68
msgid "Item Text"
msgstr ""

#: promo/models.py:69
msgid "Item Texts"
msgstr ""

#: templates/400.html:6
msgid "Bad request"
msgstr ""

#: templates/400.html:8 templates/403.html:8 templates/404.html:8
msgid "_page_details"
msgstr ""
"<p>На нашем сайте нет такой страницы. Пожалуйста, проверьте адрес страницы\n"
"или перейдите на                <a href=\"/\">главную страницу</a>.</p>"

#: templates/403.html:6
msgid "Forbidden"
msgstr ""

#: templates/404.html:6
msgid "Page not found"
msgstr "Страница не найдена"

#: templates/spec.html:53
msgid "Сброс"
msgstr ""

#: templates/theatre-eifman.html:76
msgid "Пресса о Борисе Эйфмане:"
msgstr ""

#: templates/theatre-partners.html:18
msgid "Стратегические партнёры театра"
msgstr ""

#: templates/theatre-partners.html:26
msgid "Партнёры театра"
msgstr ""

#: templates/theatre-partners.html:34
msgid "Друзья театра"
msgstr ""

#: templates/admin/hvad/edit_inline/stacked.html:10
#: templates/admin/hvad/edit_inline/tabular.html:30
msgid "View on site"
msgstr ""

#: templates/admin/hvad/edit_inline/stacked.html:68
#: templates/admin/hvad/edit_inline/tabular.html:115
#, python-format
msgid "Add another %(verbose_name)s"
msgstr ""

#: templates/admin/hvad/edit_inline/stacked.html:71
#: templates/admin/hvad/edit_inline/tabular.html:118
msgid "Remove"
msgstr ""

#: templates/admin/hvad/edit_inline/tabular.html:17
msgid "Delete?"
msgstr ""

#: templates/blocks/header.html:6
msgid ""
"Санкт-Петербургский Государственный Академический Театр Балета Бориса Эйфмана"
msgstr ""

#: templates/blocks/header.html:18 templates/blocks/header.html.py:19
msgid "Version for the visually impaired"
msgstr "Версия для слабовидящих"

#: theatre/cms_app.py:9
msgid "Ensemble App"
msgstr ""

#: theatre/cms_app.py:16
msgid "Schedule App"
msgstr ""

#: theatre/cms_app.py:23
msgid "Repertoire App"
msgstr ""

#: theatre/cms_app.py:30
msgid "Vacancy app"
msgstr ""

#: theatre/cms_plugins.py:12
#: theatre/templates/theatre/plugins/upcoming_shows.html:23
msgid "Upcoming shows"
msgstr "Ближайшие спектакли"

#: theatre/cms_plugins.py:32 theatre/models.py:378
msgid "Tales"
msgstr ""

#: theatre/cms_plugins.py:52
msgid "Page link"
msgstr ""

#: theatre/cms_plugins.py:70
msgid "Principal"
msgstr "Официальные лица"

#: theatre/cms_toolbar.py:13
msgid "Add ballet"
msgstr "Добавить балет"

#: theatre/forms.py:37 theatre/forms.py:40
msgid "Your full name"
msgstr ""

#: theatre/forms.py:43 theatre/forms.py:46
msgid "Your birth date"
msgstr ""

#: theatre/forms.py:49 theatre/forms.py:52
msgid "Your phone number"
msgstr ""

#: theatre/forms.py:55 theatre/forms.py:58
msgid "Your email"
msgstr ""

#: theatre/forms.py:61
msgid "Your achivements"
msgstr ""

#: theatre/forms.py:65
msgid "Places of study"
msgstr ""

#: theatre/forms.py:69
msgid "Places of work"
msgstr ""

#: theatre/forms.py:73
msgid "Growth, weight"
msgstr ""

#: theatre/forms.py:77
msgid "Links to your video, profile with photos"
msgstr ""

#: theatre/forms.py:81
msgid "Resume"
msgstr ""

#: theatre/forms.py:82
#, python-format
msgid "Max file size is %(file_size)s."
msgstr ""

#: theatre/forms.py:102
#, python-format
msgid "Too large file size. Max size is %(file_size)s."
msgstr ""

#: theatre/menu.py:12
msgid "Ballets menu"
msgstr "Список балетов"

#: theatre/models.py:17
msgid "Male"
msgstr ""

#: theatre/models.py:18
msgid "Female"
msgstr ""

#: theatre/models.py:22
msgid "Solist"
msgstr ""

#: theatre/models.py:23
msgid "Tutor"
msgstr ""

#: theatre/models.py:24
msgid "Corpsdeballet"
msgstr "Кордебалет"

#: theatre/models.py:25
msgid "Principial"
msgstr ""

#: theatre/models.py:29
msgid "vkontakte"
msgstr ""

#: theatre/models.py:30
msgid "facebook"
msgstr ""

#: theatre/models.py:31
msgid "twitter"
msgstr ""

#: theatre/models.py:32
msgid "instagram"
msgstr ""

#: theatre/models.py:33
msgid "youtube"
msgstr ""

#: theatre/models.py:44
msgid "First Name"
msgstr ""

#: theatre/models.py:45
msgid "Participant Name"
msgstr ""

#: theatre/models.py:46
msgid "Last Name"
msgstr ""

#: theatre/models.py:48
msgid "About person"
msgstr ""

#: theatre/models.py:50 theatre/models.py:101 theatre/models.py:362
msgid "Photo"
msgstr ""

#: theatre/models.py:51
msgid "Gender"
msgstr ""

#: theatre/models.py:52
msgid "Role"
msgstr ""

#: theatre/models.py:58 theatre/models.py:102
msgid "Person"
msgstr ""

#: theatre/models.py:59
msgid "Persons"
msgstr ""

#: theatre/models.py:106
msgid "Person photo"
msgstr ""

#: theatre/models.py:107
msgid "Person photos"
msgstr ""

#: theatre/models.py:117
msgid "Titers"
msgstr ""

#: theatre/models.py:118
msgid "Summary"
msgstr ""

#: theatre/models.py:119
msgid "Eifmans text"
msgstr ""

#: theatre/models.py:120
msgid "Libretto"
msgstr ""

#: theatre/models.py:122
msgid "Slug"
msgstr ""

#: theatre/models.py:123
msgid "Cover image"
msgstr ""

#: theatre/models.py:124
msgid "Image for schedule"
msgstr ""

#: theatre/models.py:125
msgid "If not presented, cover image will used"
msgstr ""

#: theatre/models.py:133
msgid "Ballets"
msgstr ""

#: theatre/models.py:185 theatre/models.py:186 theatre/models.py:197
#: theatre/models.py:198
msgid "Seo Fields"
msgstr ""

#: theatre/models.py:210
msgid "Ballet photo"
msgstr ""

#: theatre/models.py:211
msgid "Ballet photos"
msgstr ""

#: theatre/models.py:222
msgid "Video URL"
msgstr ""

#: theatre/models.py:227
msgid "Ballet video"
msgstr ""

#: theatre/models.py:228
msgid "Ballet videos"
msgstr ""

#: theatre/models.py:237
msgid "Author"
msgstr ""

#: theatre/models.py:239
msgid "Publisher"
msgstr ""

#: theatre/models.py:240
msgid "Publication date"
msgstr ""

#: theatre/models.py:247
msgid "Press feedback"
msgstr ""

#: theatre/models.py:248
msgid "Press feedbacks"
msgstr ""

#: theatre/models.py:261 theatre/models.py:281
msgid "City"
msgstr ""

#: theatre/models.py:264
msgid "Площадка"
msgstr ""

#: theatre/models.py:265
msgid "Площадки"
msgstr ""

#: theatre/models.py:273
msgid "Custom title"
msgstr ""

#: theatre/models.py:275
msgid "cover image"
msgstr ""

#: theatre/models.py:278
msgid "Date"
msgstr ""

#: theatre/models.py:279
msgid "Time"
msgstr ""

#: theatre/models.py:280
msgid "Place"
msgstr ""

#: theatre/models.py:282
msgid "Use when place is unknown, usually with custom title. Overrides place"
msgstr ""

#: theatre/models.py:283
msgid "Booking link"
msgstr ""

#: theatre/models.py:286
msgid "Show"
msgstr ""

#: theatre/models.py:287
msgid "Shows"
msgstr ""

#: theatre/models.py:315
msgid "Place or City must be filled"
msgstr ""

#: theatre/models.py:317
msgid "Select City OR Place"
msgstr ""

#: theatre/models.py:328
msgid "Number of shows to show"
msgstr ""

#: theatre/models.py:329
msgid "Limits the number of shows that will be displayed"
msgstr ""

#: theatre/models.py:333
msgid "Type"
msgstr ""

#: theatre/models.py:334
msgid "URL"
msgstr ""

#: theatre/models.py:343 theatre/models.py:344
msgid "Social Network"
msgstr ""

#: theatre/models.py:359
msgid "signature"
msgstr ""

#: theatre/models.py:363
msgid "Photo position"
msgstr ""

#: theatre/models.py:364
msgid "Weight"
msgstr ""

#: theatre/models.py:365
msgid "Is Published"
msgstr ""

#: theatre/models.py:377
msgid "Tale"
msgstr ""

#: theatre/models.py:382
msgid "Number of tales to show"
msgstr ""

#: theatre/models.py:383
msgid "Limits the number of tales that will be displayed"
msgstr ""

#: theatre/models.py:396
msgid "person"
msgstr ""

#: theatre/models.py:397
msgid "show photo"
msgstr ""

#: theatre/admin/balletadmin.py:41
msgid "Text in tabs"
msgstr ""

#: theatre/templates/theatre/__person_title.html:4
msgid "Труппа"
msgstr ""

#: theatre/templates/theatre/ballet.html:77
#: theatre/templates/theatre/ballet_list.html:7
msgid "Репертуар"
msgstr ""

#: theatre/templates/theatre/ballet.html:90
msgid "Премьера состоялась"
msgstr ""

#: theatre/templates/theatre/ballet.html:99
msgid "О спектакле"
msgstr ""

#: theatre/templates/theatre/ballet.html:103
msgid "Слово Б.Эйфмана"
msgstr ""

#: theatre/templates/theatre/ballet.html:107
msgid "Краткое содержание"
msgstr ""

#: theatre/templates/theatre/ballet.html:111
msgid "Видео"
msgstr ""

#: theatre/templates/theatre/ballet.html:115
msgid "Галерея"
msgstr ""

#: theatre/templates/theatre/ballet.html:119
msgid "Ближайшие спектакли"
msgstr ""

#: theatre/templates/theatre/ballet.html:123
msgid "Пресса"
msgstr ""

#: theatre/templates/theatre/ensemble_index.html:4
#, fuzzy
msgid "Труппа русского балета Бориса Эйфмана"
msgstr ""
"Русский балет | Санкт-Петербургский новый, современный театр классического "
"балета в Александринском театре балета | официальный сайт театра русского "
"балета Бориса Эйфмана"

#: theatre/templates/theatre/ensemble_index.html:13
msgid "Солисты"
msgstr ""

#: theatre/templates/theatre/ensemble_index.html:39
msgid "Педагоги и репетиторы"
msgstr ""

#: theatre/templates/theatre/ensemble_index.html:63
msgid "Кордебалет"
msgstr ""

#: theatre/templates/theatre/schedule.html:4
#, fuzzy
msgid "Афиша | новый театр русского балета Бориса Эйфмана"
msgstr ""
"Русский балет | Санкт-Петербургский новый, современный театр классического "
"балета в Александринском театре балета | официальный сайт театра русского "
"балета Бориса Эйфмана"

#: theatre/templates/theatre/schedule.html:35
msgid "Афиша"
msgstr ""

#: theatre/templates/theatre/schedule.html:48
msgid "В этом месяце пока что ничего нет"
msgstr ""

#: theatre/templates/theatre/vacancy.html:11
msgid ""
"Раздел предназначен для соискателей на замещение вакантных должностей "
"артистов и солистов балета"
msgstr ""

#: theatre/templates/theatre/vacancy_success.html:12
msgid "Ваше сообщение отправлено."
msgstr ""

#: theatre/templates/theatre/inc/schedule_item.html:47
msgid "Подробнее о спектакле"
msgstr ""

#: theatre/templates/theatre/inc/schedule_item.html:50
#: theatre/templates/theatre/plugins/upcoming_shows.html:70
msgid "Заказ билетов"
msgstr ""

#: youtubeurl_field/formfields.py:10
msgid "Enter a valid URL."
msgstr ""

#: youtubeurl_field/modelfields.py:14
msgid "YouTube url"
msgstr ""

#: youtubeurl_field/validators.py:20
msgid "Video is not available"
msgstr ""

#: youtubeurl_field/validators.py:22 youtubeurl_field/validators.py:25
#: youtubeurl_field/validators.py:27
msgid "Not a valid Youtube URL"
msgstr ""
