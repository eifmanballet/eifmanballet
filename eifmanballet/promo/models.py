# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from filer.fields.image import FilerImageField
from hvad.models import TranslatableModel, TranslatedFields
from model_utils.models import TimeStampedModel
from theatre.models import Ballet


class Item(TranslatableModel, TimeStampedModel):
    translations = TranslatedFields(

    )
    image = FilerImageField(verbose_name=_("Normal Image"), related_name="image")
    blur = FilerImageField(verbose_name=_("Blurred Image"), related_name="blur")
    ballet = models.ForeignKey(Ballet, verbose_name=_("Ballet"))
    width = models.PositiveIntegerField(_("width"), null=True, blank=True)
    height = models.PositiveIntegerField(_("height"), null=True, blank=True)
    handler_coords = models.CharField(_("Handler coordinates"), max_length=255, null=True, blank=True)
    image_coords = models.CharField(_("Image coordinates"), max_length=32, default="0,0")
    row = models.PositiveIntegerField(_("Row number"), default=1)
    position = models.PositiveIntegerField(_("Position in row"), default=0)

    class Meta:
        verbose_name = _("Item")
        verbose_name_plural = _("Items")
        ordering = (
            "position",
        )

    def __unicode__(self):
        return self.ballet.__unicode__()

    @property
    def get_handler_coordinates(self):
        if self.handler_coords == "":
            left = top = 0
            width = self.image.width
            height = self.image.height
        else:
            left, top, width, height = self.handler_coords.split(",")
        return {
            "left": left,
            "top": top,
            "width": width,
            "height": height,
        }

    @property
    def get_image_coordinates(self):
        left, top = self.image_coords.split(",")
        return {
            "left": left,
            "top": top,
        }


class ItemText(TranslatableModel, TimeStampedModel):
    translations = TranslatedFields(
        text=models.CharField(_("text"), max_length=255),
        color=models.CharField(_("Color"), max_length=7, default="#FFF"),
        font_size=models.PositiveIntegerField(_("Font size"), null=True, blank=True),
        position=models.CharField(_("Position"), max_length=32, default="0,0")
    )
    item = models.ForeignKey(Item, verbose_name=_("Item"), related_name="texts")

    class Meta:
        verbose_name = _("Item Text")
        verbose_name_plural = _("Item Texts")
        ordering = []

    def __unicode__(self):
        text = self.lazy_translation_getter('text')
        return u"{} ()".format(text, self.item)

    @property
    def get_position(self):
        left, top = self.lazy_translation_getter('position').split(",")
        return {
            "left": left,
            "top": top,
        }
