# coding=utf-8
from django.contrib import admin
from hvad.admin import TranslatableAdmin, TranslatableTabularInline
from promo.models import Item, ItemText


class ItemTextInline(TranslatableTabularInline):
    model = ItemText
    extra = 0


class ItemAdmin(TranslatableAdmin):
    list_display = ['ballet', 'row', 'position',]
    list_editable = ['row', 'position',]
    ordering = ['row', 'position',]
    inlines = [
        ItemTextInline
    ]

admin.site.register(Item, ItemAdmin)
