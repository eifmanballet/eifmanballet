# coding=utf-8 
from django.utils.translation import ugettext_lazy as _
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase
from promo.models import Item


@plugin_pool.register_plugin
class CMSPromoBlock(CMSPluginBase):
    name = _("Promo block")
    allow_children = None
    module = 'Theatre'
    render_template = "promo/plugins/promo.html"

    def render(self, context, instance, placeholder):
        qs = Item.objects.order_by("position")

        context.update({
            'instance': instance,
            'items': qs,
            'object_list': qs,
            'placeholder': placeholder,
        })

        return context
