# coding=utf-8
from datetime import datetime
from fabric.api import *
from fabric.colors import red

env.hosts = ['eifmanballet@eifmanballet.ru', ]
env.shell = "/bin/sh -c"
base_dir = '/pub/home/eifmanballet/eifmanballet-ru/new'
code_dir = base_dir + '/htdocs'
db_dir = code_dir + '/databases'
db_name = 'db.sqlite3'
env_file = base_dir + '/.envs/eifman/bin/python'
pip_file = base_dir + '/.envs/eifman/bin/pip'
wsgi_file = base_dir + '/index.wsgi'
ENV_PARAMS = {
    "SECRET_KEY": 'cVXHX6GxeF5eszgu3OZBjBtF3rVDXEtR',
    "DJANGO_SETTINGS_MODULE": 'eifmanballet.settings.production',
}
UNSYNCED_MSG = red('''Repo is unsynced. Please push/pull and try again''') + '''

Unsynced commits:
{}'''


@task()
def deploy():
    check_repo()
    backup_db()
    stop_service()
    update()
    migrate()
    start_service()


@task
def check_repo():
    status = local('git log origin/master..HEAD --oneline', capture=True)
    print status

    if len(status):
        msg = UNSYNCED_MSG.format(status)
        abort(msg)


@task
def backup_db():
    with cd(db_dir):
        ts = datetime.now().strftime("%Y%m%d_%H%M%S")
        run('cp {0} {0}.{1}'.format(db_name, ts))


@task
def update():
    with cd(code_dir):
        run('git pull')
        with shell_env(**ENV_PARAMS):
            run('{} ./eifmanballet/manage.py collectstatic --noinput'.format(env_file))


@task
def migrate():
    with cd(code_dir):
        with shell_env(**ENV_PARAMS):
            run('{} ./eifmanballet/manage.py migrate'.format(env_file))


@task
def stop_service():
    pass


@task
def start_service():
    run("touch {}".format(wsgi_file))


@task
def restart(host='127.0.0.1', port='8000'):
    stop_service()
    start_service(host, port)


@task
def update_requirements(environ='staging'):
    check_repo()
    with cd(code_dir):
        run('{pip} install -r requirements/{env}.txt'.format(**{
            'pip': pip_file,
            'env': environ
        }))